Shader "Kudos/Laser"
{
    Properties
    {
        [PerRenderData] _ColorLaz("Color", Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
        _LazerSmooth("Lazer smooth", Float) = 1
        _LazerThicknesss("Lazer thick", Float) = 1
        _LazerSpeed("Lazer speed", Vector) = (0,0,0,0)
        _TransparentValue("Alpha Value", Range(0,1)) = 1
        [PerRenderData]_EmissionLaz("Emission", Float) = 1
        _ClipLazer("Clippin lazer", Range(0.0, 1.0)) = 0.0 
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}

        ZWrite off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma multi_compile_instancing
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                fixed4 uv : TEXCOORD0;
                fixed4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            fixed2 rand2dTo2d(fixed2 value) 
            {
                fixed2 smallValue = sin(value);
                fixed random = dot(smallValue, fixed2(12.989h, 78.233h));
                random = frac(sin(random) * 143758.5453h);

                fixed2 res = fixed2(random, 0);

                random = dot(smallValue, fixed2(39.346h, 11.135h));
                random = frac(sin(random) * 143758.5453h);
                
                res.y = random;
                return res;
            }

            // voronoi noise function
            fixed voronoiNoise(fixed2 value)
            {
                fixed2 baseCell = floor(value);

                fixed minDistToCell = 5.0h;

                [unroll]
                for (int x = -1; x <= 1; x++)
                {
                    [unroll]
                    for (int y = -1; y <= 1; y++)
                    {
                        fixed2 cell = baseCell + fixed2(x, y);
                        fixed2 cellPosition = cell + rand2dTo2d(cell);
                        fixed2 toCell = cellPosition - value;
                        fixed distToCell = length(toCell);

                        if (distToCell < minDistToCell)
                        {
                            minDistToCell = distToCell;
                        }
                    }
                }
                return minDistToCell;
            }

            sampler2D _MainTex;
            uniform fixed4 _MainTex_ST;
            uniform fixed _LazerSmooth;
            //uniform fixed4 _ColorLaz;
            uniform fixed _LazerThicknesss;
            uniform fixed2 _LazerSpeed;
            uniform fixed _TransparentValue;
            //uniform fixed _EmissionLaz;
            uniform fixed _ClipLazer;

            UNITY_INSTANCING_BUFFER_START(Props)
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _ColorLaz)
                UNITY_DEFINE_INSTANCED_PROP(fixed, _EmissionLaz)
            UNITY_INSTANCING_BUFFER_END(Props)

            v2f vert (appdata v)
            {
                UNITY_SETUP_INSTANCE_ID(v);
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = UNITY_ACCESS_INSTANCED_PROP(Props, _ColorLaz) * UNITY_ACCESS_INSTANCED_PROP(Props, _EmissionLaz);
                o.uv.z = 0.16h + voronoiNoise((_LazerSpeed * (fixed2)_Time.y) + o.uv.xy) * 0.8h;
                o.color.a = 1.0h - o.uv.y;
                UNITY_TRANSFER_INSTANCE_ID(v, o);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                fixed lazer = pow(1.0h - pow(i.color.a, _LazerSmooth) - pow(i.uv.y, _LazerSmooth), _LazerThicknesss) * i.uv.z;
                if (lazer < _ClipLazer || i.uv.y < 0.1h || i.uv.y > 0.9h) lazer = 0.0h;
                fixed alphaLazer = lazer * _TransparentValue;
                fixed3 col = (fixed3)lazer * i.color.rgb;
                return fixed4(col, alphaLazer);
            }

            ENDCG
        }
    }
}
