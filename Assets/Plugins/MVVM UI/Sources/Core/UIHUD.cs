﻿namespace MVVMUI
{
    public abstract class UIHUD : UIBindable
    {
        public static string OnAppearingMethodName => nameof(OnAppearing);
        public static string OnDisappearingMethodName => nameof(OnDisappearing);

        protected virtual void OnAppearing() { }
        protected virtual void OnDisappearing() { }
    }
}