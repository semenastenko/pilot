using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace MVVMUI
{
    public abstract class UIPanel : UIBindable
    {
        public static string OnAppearingMethodName => nameof(OnAppearing);
        public static string OnDisappearingMethodName => nameof(OnDisappearing);
        public static string OnTransitionMethodName => nameof(OnTransition);

        protected virtual void OnAppearing() { }
        protected virtual void OnDisappearing() { }
        protected virtual async Task OnTransition() => await Task.Yield();
    }
}