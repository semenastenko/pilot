﻿using System.Threading.Tasks;
using UnityEngine;

namespace MVVMUI
{
    public abstract class UIPopup : UIBindable
    {
        public static string WaitBusyMethodName => nameof(WaitBusy);
        public static string OnAppearingMethodName => nameof(OnAppearing);
        public static string OnDisappearingMethodName => nameof(OnDisappearing);
        public static string OnTransitionMethodName => nameof(OnTransition);

        protected bool isBusy;
        protected object result;

        protected virtual void OnAppearing() { }
        protected virtual void OnDisappearing() { }
        protected virtual async Task OnTransition() => await Task.Yield();

        protected async Task<object> WaitBusy()
        {
            isBusy = true;
            while (isBusy && Application.isPlaying)
            {
                await Task.Yield();
            }
            var waitResult = result;
            result = null;
            return waitResult;
        }

        protected void SetNoBusy() => isBusy = false;
        protected void SetResult(object value)
        {
            result = value;
            SetNoBusy();
        }
    }
}