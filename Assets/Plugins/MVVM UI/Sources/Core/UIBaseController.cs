using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace MVVMUI
{
    public abstract class UIBaseController
    {
        protected struct CanvasContainer
        {
            public Canvas canvas;
            public Transform panelsContainer;
            public Transform popupsContainer;
            public Transform hudsContainer;
        }

        protected readonly CanvasContainer _container;
        protected readonly List<UIPanel> _panels;
        protected readonly List<UIPopup> _popups;
        protected readonly List<UIHUD> _HUDs;

        public ReadOnlyCollection<UIPanel> Panels => _panels.AsReadOnly();
        public ReadOnlyCollection<UIPopup> Popups => _popups.AsReadOnly();
        public ReadOnlyCollection<UIHUD> HUDs => _HUDs.AsReadOnly();

        public IEnumerable<UIBindable> Bindables => 
            _panels.Select(x=> (UIBindable)x)
            .Union(_popups.Select(x => (UIBindable)x))
            .Union(_HUDs.Select(x => (UIBindable)x));

        public UIBaseController(Canvas canvas)
        {
            var bindables = Object.FindObjectsOfType<UIBindable>(true);

            foreach (var item in bindables)
            {
                item.InitializeBindings();
                item.gameObject.SetActive(false);
            }

            _container.canvas = canvas;
            _panels = new List<UIPanel>(bindables.Where(x => x is UIPanel).Cast<UIPanel>());
            _popups = new List<UIPopup>(bindables.Where(x => x is UIPopup).Cast<UIPopup>());
            _HUDs = new List<UIHUD>(bindables.Where(x => x is UIHUD).Cast<UIHUD>());

            _container.hudsContainer = new GameObject(nameof(_container.hudsContainer), typeof(RectTransform)).transform;
            _container.hudsContainer.SetParent(_container.canvas.transform);
            ResetRectTransform((RectTransform)_container.hudsContainer);

            foreach (var item in _HUDs)
            {
                item.transform.SetParent(_container.hudsContainer);
            }

            _container.panelsContainer = new GameObject(nameof(_container.panelsContainer), typeof(RectTransform)).transform;
            _container.panelsContainer.SetParent(_container.canvas.transform);
            ResetRectTransform((RectTransform)_container.panelsContainer);

            foreach (var item in _panels)
            {
                item.transform.SetParent(_container.panelsContainer);
            }

            _container.popupsContainer = new GameObject(nameof(_container.popupsContainer), typeof(RectTransform)).transform;
            _container.popupsContainer.SetParent(_container.canvas.transform);
            ResetRectTransform((RectTransform)_container.popupsContainer);

            foreach (var item in _popups)
            {
                item.transform.SetParent(_container.popupsContainer);
            }
        }

        private void ResetRectTransform(RectTransform rectTransform)
        {
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.sizeDelta = Vector2.zero;
            rectTransform.anchoredPosition = Vector2.zero;
        }
    }
}
