﻿namespace MVVMUI
{
    public enum BindingMode
    {
        //
        // Summary:
        //     Indicates that the binding should only propagate changes from source (usually
        //     the View Model) to target (the BindableObject). This is the default mode for
        //     most BindableProperty values.
        OneWay,
        //
        // Summary:
        //     Indicates that the binding should propagates changes from source (usually the
        //     View Model) to target (the BindableObject) in both directions.
        TwoWay,
        
        //
        // Summary:
        //     Indicates that the binding should only propagate changes from target (the BindableObject)
        //     to source (usually the View Model). This is mainly used for read-only BindableProperty
        //     values.
        OneWayToSource,
        //
        // Summary:
        //     Indicates that the binding will be applied only when the binding context changes
        //     and the value will not be monitored for changes with INotifyPropertyChanged.
        //OneTime
    }
}