﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace MVVMUI
{
    public struct BindableProperty
    {
        public Action<object> setter;
        public Func<object> getter;

        public BindableProperty(Action<object> setter, Func<object> getter)
        {
            this.setter = setter;
            this.getter = getter;
        }
    }
    public interface IBindableObject : INotifyPropertyChanged
    {
        Dictionary<string, BindableProperty> GetBindableProperties();
    }
}