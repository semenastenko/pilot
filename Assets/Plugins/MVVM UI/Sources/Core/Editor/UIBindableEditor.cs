using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Reflection;
using UnityEngine.UIElements;

namespace MVVMUI.Editor
{
    [CustomEditor(typeof(UIBindable), true)]
    public class UIBindableEditor : UnityEditor.Editor
    {
        SerializedProperty bindingsProperty;
        IEnumerable<PropertyInfo> propertyInfos;

        private void OnEnable()
        {
            bindingsProperty = serializedObject.FindProperty("m_Bindings");
            var uiBindable = (UIBindable)target;
            
            var context = uiBindable.GetBindingContextType();
            var gameObjectPropertires = typeof(MonoBehaviour).GetProperties().Select(x=> x.Name);
            propertyInfos = context.GetProperties()
                .Where(x=> !gameObjectPropertires.Contains(x.Name) && x.Name != nameof(UIBindable.BindingContext));
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));
            EditorGUILayout.TextField(new GUIContent("Binding Context"), ((UIBindable)target).GetBindingContextType().Name);
            EditorGUI.EndDisabledGroup();
            GUILayout.Space(5);
            bindingsProperty.isExpanded = EditorGUILayout.Foldout(bindingsProperty.isExpanded, bindingsProperty.displayName);

            if (bindingsProperty.isExpanded)
            {
                EditorGUI.indentLevel += 1;
                for (int i = 0; i < bindingsProperty.arraySize; i++)
                {
                    GUILayout.BeginHorizontal();
                    DrawBindingsProperty(bindingsProperty.GetArrayElementAtIndex(i));
                    if (GUILayout.Button("-", EditorStyles.miniButtonMid, GUILayout.Width(20f)))
                    {
                        bindingsProperty.DeleteArrayElementAtIndex(i);
                        break;
                    }
                    GUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel -= 1;
            }
            GUILayout.Space(5);
            if (GUILayout.Button("Add"))
            {
                bindingsProperty.isExpanded = true;
                bindingsProperty.InsertArrayElementAtIndex(bindingsProperty.arraySize);
                var lastProp = bindingsProperty.GetArrayElementAtIndex(bindingsProperty.arraySize - 1);
                lastProp.FindPropertyRelative(nameof(BindableObject.bindable)).objectReferenceValue = null;
                lastProp.FindPropertyRelative(nameof(BindableObject.properties)).ClearArray();
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawBindingsProperty(SerializedProperty property)
        {
            GUILayout.BeginVertical();
            var bindableProperty = property.FindPropertyRelative(nameof(BindableObject.bindable));
            var foldoutName = property.displayName;
            if (bindableProperty.objectReferenceValue != null)
                foldoutName = bindableProperty.objectReferenceValue.name;
            property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, foldoutName);
            if (property.isExpanded)
            {
                var items = GetProperties(bindableProperty);

                EditorGUILayout.PropertyField(bindableProperty, new GUIContent("Binding"));

                DrawPropertiesList(property.FindPropertyRelative(nameof(BindableObject.properties)), items);
            }
            GUILayout.EndVertical();
        }

        private string[] GetProperties(SerializedProperty property)
        {
            var reference = property.objectReferenceValue as GameObject;
            if (reference != null)
            {
                var bindable = reference.GetComponent<IBindableObject>();
                if (bindable != null)
                {
                    return bindable.GetBindableProperties().Select(x => x.Key).ToArray();
                }
            }
            return new string[0];
        }

        private void DrawPropertiesList(SerializedProperty property, string[] items)
        {
            if (!property.isArray) return;
            EditorGUI.indentLevel += 1;
            for (int i = 0; i < property.arraySize; i++)
            {
                var prop = property.GetArrayElementAtIndex(i);
                EditorGUILayout.BeginHorizontal();
                DrawBindingProperty(prop);
                if (GUILayout.Button(new GUIContent("-"), EditorStyles.miniButtonMid, GUILayout.Width(20f)))
                {
                    property.DeleteArrayElementAtIndex(i);
                    break;
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUI.indentLevel -= 1;
            List<string> uncontainedItems = new List<string>();
            foreach (var item in items)
            {
                if (!ContainsInArrayPorperty(property, item))
                {
                    uncontainedItems.Add(item);
                }
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (uncontainedItems.Count != 0 && EditorGUILayout.DropdownButton(new GUIContent("Add property"), FocusType.Passive, EditorStyles.miniPullDown, GUILayout.Width(100f)))
            {
                GenericMenu menu = new GenericMenu();
                foreach (var item in uncontainedItems)
                {
                    menu.AddItem(new GUIContent(item), false, () => OnSelectMenu(item, property));
                }
                menu.ShowAsContext();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawBindingProperty(SerializedProperty property)
        {
            EditorGUILayout.BeginVertical();
            var bindingProperty = property.FindPropertyRelative(nameof(BindingProperty.target));
            var foldoutName = bindingProperty.stringValue;
            property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, foldoutName);

            if (property.isExpanded)
            {
                DrawPopup(property.FindPropertyRelative(nameof(BindingProperty.sourse)));
                var modePorperty = property.FindPropertyRelative(nameof(BindingProperty.bindingMode));
                EditorGUILayout.PropertyField(modePorperty, new GUIContent("Mode", modePorperty.tooltip));
            }
            EditorGUILayout.EndVertical();
        }

        private void DrawPopup(SerializedProperty property)
        {
            EditorGUILayout.BeginHorizontal();
            var elements = new List<string>();
            elements.Add("None");
            elements.AddRange(propertyInfos.Select(x => x.Name));
            var selectedIndex = elements.FindIndex(x => x == property.stringValue);
            if (selectedIndex < 0)
                selectedIndex = 0;
            var index = EditorGUILayout.Popup(
            selectedIndex,
            elements.ToArray()
            );

            if (index < elements.Count && index >= 0)
                property.stringValue = elements[index];
            EditorGUILayout.EndHorizontal();
        }

        private bool ContainsInArrayPorperty(SerializedProperty property, string item)
        {
            if (!property.isArray) return false;
            for (int i = 0; i < property.arraySize; i++)
            {
                var prop = property.GetArrayElementAtIndex(i).FindPropertyRelative(nameof(BindingProperty.target));
                if (prop == null) return false;
                if (prop.stringValue == item)
                    return true;
            }
            return false;
        }

        private void OnSelectMenu(string item, SerializedProperty property)
        {
            if (!property.isArray) return;
            property.serializedObject.Update();
            property.InsertArrayElementAtIndex(property.arraySize);
            var lastProp = property.GetArrayElementAtIndex(property.arraySize - 1);
            lastProp.FindPropertyRelative(nameof(BindingProperty.target)).stringValue = item;
            property.serializedObject.ApplyModifiedProperties();
        }
    }
}