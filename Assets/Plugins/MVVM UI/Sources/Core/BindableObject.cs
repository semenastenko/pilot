﻿using UnityEngine;

namespace MVVMUI
{
    [System.Serializable]
    public struct BindableObject
    {
        public GameObject bindable;
        public BindingProperty[] properties;
    }

    [System.Serializable]
    public struct BindingProperty
    {
        /// <summary>
        /// the BindableObject
        /// </summary>
        public string target;
        /// <summary>
        /// the View Model
        /// </summary>
        public string sourse;
        [Tooltip("OneWay - Indicates that the binding should only propagate changes from source (usuallythe View Model) to target (the BindableObject). This is the default mode for most BindableProperty values.\n\n" +
            "TwoWay - Indicates that the binding should propagates changes from source (usually the View Model) to target (the BindableObject) in both directions.\n\n" +
            "OneWayToSource - Indicates that the binding should only propagate changes from target (the BindableObject) to source (usually the View Model). This is mainly used for read-only BindableProperty values.")]
        public BindingMode bindingMode;
    }
}