using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace MVVMUI
{
    public abstract class UIBindable : MonoBehaviour, INotifyPropertyChanged
    {
        [SerializeField] List<BindableObject> m_Bindings = new();

        private object m_BindingContext;
        private Dictionary<string, Action> notifyRequests;
        private bool isInitialize;

        public object BindingContext
        {
            get => m_BindingContext;
            set => m_BindingContext = value;
        }

        protected virtual bool SetProperty<T>(
            ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null,
            Func<T, T, bool> validateValue = null)
        {
            //if value didn't change
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            //if value changed but didn't validate
            if (validateValue != null && !validateValue(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "") =>
         PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public abstract Type GetBindingContextType();

        public virtual void OnInitialize() { }

        public void InitializeBindings()
        {
            if (isInitialize) return;

            isInitialize = true;

            OnInitialize();

            var contextType = GetBindingContextType();

            if (contextType.IsSubclassOf(typeof(UnityEngine.Object)))
            {
                BindingContext = FindObjectOfType(contextType, true);
            }
            else
            {
                BindingContext = contextType
                    .GetConstructor(new Type[0])?
                    .Invoke(new object[0]);
            }

            var notifyProperty = BindingContext as INotifyPropertyChanged;

            if (BindingContext == null)
                throw new Exception($"Invalid in {name} BindingContext is null");
            else if (notifyProperty == null)
                throw new Exception($"Invalid in {name} BindingContext ({BindingContext}) not as INotifyPropertyChanged");

            notifyRequests = new Dictionary<string, Action>();

            foreach (var binding in m_Bindings)
            {
                var bindable = binding.bindable.GetComponent<IBindableObject>();
                if (bindable == null) throw new Exception("Bindable object is null");
                var dictionary = bindable.GetBindableProperties();
                foreach (var property in binding.properties)
                {
                    if (dictionary.TryGetValue(property.target, out var bindableProperty))
                    {

                        var sourseInfo = BindingContext.GetType().GetProperty(property.sourse);
                        if (sourseInfo == null)
                        {
                            Debug.LogWarning($"View {this.name} typeof({GetType().Name}) has binding '{property.target}' not selected property");
                            return;
                        }
                        if (property.bindingMode == BindingMode.OneWay || property.bindingMode == BindingMode.TwoWay)
                        {
                            void action()
                            {
                                if (bindableProperty.setter != null)
                                    bindableProperty.setter.Invoke(sourseInfo.GetValue(BindingContext));
                            }
                            notifyRequests.Add(property.sourse, action);
                            action();
                        }
                        if (property.bindingMode == BindingMode.OneWayToSource || property.bindingMode == BindingMode.TwoWay)
                        {
                            bindable.PropertyChanged += (e, arg) =>
                            {
                                if (bindableProperty.getter != null)
                                    sourseInfo.SetValue(BindingContext, bindableProperty.getter.Invoke());
                            };
                        }
                    }
                    else
                    {
                        throw new Exception($"Bindable object not contains '{property.target}' property");
                    }
                }

                notifyProperty.PropertyChanged += NotifyProperty_PropertyChanged;
            }

        }

        private void NotifyProperty_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            notifyRequests.TryGetValue(e.PropertyName, out var notify);

            notify?.Invoke();
        }
    }
}