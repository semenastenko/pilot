﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace MVVMUI
{
    public class UIController : UIBaseController
    {
        protected UIPanel _currentPanel;
        protected List<UIPopup> _openPopups = new List<UIPopup>();
        protected List<UIHUD> _openHUDs = new List<UIHUD>();

        public static UIController Current { get; protected set; }

        public ReadOnlyCollection<UIPopup> OpenPopups => _openPopups.AsReadOnly();
        public ReadOnlyCollection<UIHUD> OpenHUDs => _openHUDs.AsReadOnly();
        public UIPanel CurrentPanel
        {
            get => _currentPanel;
            protected set
            {
                if (_currentPanel != null && _currentPanel != value)
                {
                    _currentPanel.gameObject.SetActive(false);
                    _currentPanel.GetType()
                        .GetMethod(UIPanel.OnDisappearingMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                        .Invoke(_currentPanel, new object[0]);
                }
                _currentPanel = value;
            }
        }

        public UIController(Canvas canvas) : base(canvas)
        {
            Current = this;
        }

        public virtual async Task GoToPanelAsync<Panel>() where Panel : UIPanel
        {
            await GoToPanelAsync<Panel>(null);
        }

        public virtual async Task GoToPanelAsync(System.Type type)
        => await GoToPanelAsync(type, null);

        public virtual async Task GoToPanelAsync<Panel>(params (string property, object value)[] args) where Panel : UIPanel
        => await GoToPanelAsync(typeof(Panel), args);

        public virtual async Task GoToPanelAsync(System.Type type, params (string property, object value)[] args)
        {
            var panel = _panels.Find(x => type.Equals(x.GetType()));
            if (panel == null || panel == CurrentPanel) return;
            panel.transform.SetAsLastSibling();
            if (args != null)
            {
                var properties = type.GetProperties().ToList();
                foreach (var arg in args)
                {
                    var panelProperty = properties.Find(x => x.Name == arg.property);

                    if (panelProperty != null)
                        panelProperty.SetValue(panel, arg.value);
                }
            }
            panel.gameObject.SetActive(true);
            panel.GetType()
                .GetMethod(UIPanel.OnAppearingMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                .Invoke(panel, new object[0]);
            await (Task)panel.GetType()
                .GetMethod(UIPanel.OnTransitionMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                .Invoke(panel, new object[0]);

            CurrentPanel = panel;
        }

        public virtual void SetActive<Panel>() where Panel : UIPanel
        => SetActive(typeof(Panel), null);

        public virtual void SetActive<Panel>(params (string property, object value)[] args) where Panel : UIPanel
        => SetActive(typeof(Panel), args);

        public virtual void SetActive(System.Type type)
            => SetActive(type, null);

        public virtual void SetActive(System.Type type, params (string property, object value)[] args)
        {
            var panel = _panels.Find(x => type.Equals(x.GetType()));
            if (panel == null || panel == CurrentPanel) return;
            if (args != null)
            {
                var properties = type.GetProperties().ToList();
                foreach (var arg in args)
                {
                    var panelProperty = properties.Find(x => x.Name == arg.property);

                    if (panelProperty != null)
                        panelProperty.SetValue(panel, arg.value);
                }
            }
            panel.transform.SetAsLastSibling();
            panel.gameObject.SetActive(true);
            CurrentPanel = panel;
        }

        public virtual async Task<object> DisplayPopup<Popup>(params (string property, object value)[] args) where Popup : UIPopup
        => await DisplayPopup(typeof(Popup), args);

        public virtual async Task<object> DisplayPopup(System.Type type, params (string property, object value)[] args)
        {
            var popup = _popups.Find(x => type.Equals(x.GetType()));
            if (popup == null) return null;
            popup.transform.SetAsLastSibling();
            if (args != null)
            {
                var properties = type.GetProperties().ToList();
                foreach (var arg in args)
                {
                    var panelProperty = properties.Find(x => x.Name == arg.property);

                    if (panelProperty != null)
                        panelProperty.SetValue(popup, arg.value);
                }
            }
            popup.gameObject.SetActive(true);

            popup.GetType()
               .GetMethod(UIPopup.OnAppearingMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
               .Invoke(popup, new object[0]);

            await (Task)popup.GetType()
                .GetMethod(UIPopup.OnTransitionMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                .Invoke(popup, new object[0]);

            _openPopups.Add(popup);
            var result = await (Task<object>)popup.GetType()
                .GetMethod(UIPopup.WaitBusyMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                .Invoke(popup, new object[0]);

            popup.gameObject.SetActive(false);
            popup.GetType()
              .GetMethod(UIPopup.OnDisappearingMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
              .Invoke(popup, new object[0]);

            _openPopups.Remove(popup);
            return result;
        }

        public virtual void AddHUD<HUD>(int order = 0, bool inverseOrder = false) where HUD : UIHUD
        {
            var hud = _HUDs.Find(x => x is HUD);
            if (hud == null) return;
            var siblingCount = hud.transform.parent.childCount;

            var orderIndex = 0;
            if (inverseOrder)
                orderIndex = order;
            else
                orderIndex = siblingCount - order;

            if (orderIndex <= 0)
                hud.transform.SetAsLastSibling();
            else if (orderIndex >= siblingCount)
                hud.transform.SetAsFirstSibling();
            else
                hud.transform.SetSiblingIndex(orderIndex);

            if (!_openHUDs.Contains(hud))
            {
                hud.gameObject.SetActive(true);

                hud.GetType()
                   .GetMethod(UIHUD.OnAppearingMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                   .Invoke(hud, new object[0]);

                _openHUDs.Add(hud);
            }
        }

        public virtual void RemoveHUD<HUD>() where HUD : UIHUD
        {
            var hud = _HUDs.Find(x => x is HUD);
            if (hud == null) return;

            if (_openHUDs.Contains(hud))
            {
                hud.gameObject.SetActive(false);
                hud.GetType()
                   .GetMethod(UIHUD.OnDisappearingMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                   .Invoke(hud, new object[0]);

                _openHUDs.Remove(hud);
            }
        }

        public virtual void RemoveAllHUD()
        {
            foreach (var hud in _openHUDs)
            {
                hud.gameObject.SetActive(false);
                hud.GetType()
                  .GetMethod(UIHUD.OnDisappearingMethodName, BindingFlags.NonPublic | BindingFlags.Instance)
                  .Invoke(hud, new object[0]);
            }

            _openHUDs.Clear();
        }
    }
}
