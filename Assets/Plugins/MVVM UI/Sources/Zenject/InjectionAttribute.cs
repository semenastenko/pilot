using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MVVMUI.Zenject
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectionAttribute : Attribute
    {
        public InjectionAttribute()
        {
        }
    }
}