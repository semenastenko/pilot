﻿using System;
using System.Linq;
using Zenject;

namespace MVVMUI.Zenject
{
    public static class InjectionExtensions
    {
        public static void BindBindingContext(this DiContainer container, object context)
        {
            var attributes = context.GetType().GetCustomAttributes(false)?.Where(x => x is InjectionAttribute);
            if (attributes == null || !attributes.Any()) return;

            container.Bind(context.GetType()).FromInstance(context);
            container.QueueForInject(context);
        }

        public static void BindBindingContext<T>(this DiContainer container, object context)
        {
            var attributes = context.GetType().GetCustomAttributes(false)?.Where(x => x is InjectionAttribute);
            if (attributes == null || !attributes.Any()) return;

            container.Bind<T>().FromInstance((T)context);
            container.QueueForInject((T)context);
        }
    }
}