﻿using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

namespace MVVMUI.Elements
{
    [AddComponentMenu("UI/MVVM/SliderBar")]
    public class SliderBar : Slider, IBindableObject
    {
        public const string TextProperty = "Value";

        public event PropertyChangedEventHandler PropertyChanged;

        public override float value
        {
            get => base.value;
            set
            {
                base.value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.value)));
            }
        }

        private void SetValue(object arg)
        {
            float val = 0;
            try
            {
                val = System.Convert.ToSingle(arg);
            }
            catch 
            {
                return; 
            }
            value = val;
        }

        private object GetValue()
        => value;

        public Dictionary<string, BindableProperty> GetBindableProperties()
        {
            var properties = new Dictionary<string, BindableProperty>();
            properties.Add(TextProperty, new BindableProperty(SetValue, GetValue));
            return properties;
        }
    }
}