using MvvmHelpers.Commands;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace MVVMUI.Elements
{
    [AddComponentMenu("UI/MVVM/Button")]
    public class TextButton : Button, IBindableObject
    {
        public const string TextProperty = "Text";
        public const string CommandProperty = "Command";
        [SerializeField] Text m_TextComponent;
        [SerializeField, TextArea(3, 10)] protected string m_ButtonText;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void InvokePropertyChanged(object sender, PropertyChangedEventArgs e)
        => PropertyChanged?.Invoke(sender, e);

        public virtual string Text
        {
            get => m_ButtonText;
            set
            {
                m_ButtonText = value;
                if (m_TextComponent != null)
                {
                    m_TextComponent.text = m_ButtonText;
                }
                if (m_ButtonText == value) return;
                InvokePropertyChanged(this, new PropertyChangedEventArgs(nameof(Text)));
            }
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            Text = m_ButtonText;
        }
#endif
        protected void SetText(object obj)
        {
            var value = obj as string;
            if (value == null) return;
            Text = value;
        }

        protected object GetText() => Text;

        protected void OnClick(object obj)
        {
            if (obj is AsyncCommand)
            {
                onClick.AddListener(async () =>
                {
                    interactable = false;
                    await ((AsyncCommand)obj).ExecuteAsync();
                    interactable = true;
                });
            }
            else if (obj is Command)
            {
                onClick.AddListener(() => ((Command)obj).Execute(null));
            }
        }

        public Dictionary<string, BindableProperty> GetBindableProperties()
        {
            var properties = new Dictionary<string, BindableProperty>();
            properties.Add(TextProperty, new BindableProperty(SetText, GetText));
            properties.Add(CommandProperty, new BindableProperty(OnClick, null));
            return properties;
        }
    }
}