﻿using MvvmHelpers.Commands;
using System.Collections.Generic;
using System.ComponentModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MVVMUI.Elements
{
    [AddComponentMenu("UI/MVVM/TMPButton")]
    public class TMPButton : TextButton
    {
        [SerializeField] TMP_Text m_TMPTextComponent;
        public override string Text
        {
            get => m_ButtonText;
            set
            {
                m_ButtonText = value;
                if (m_TMPTextComponent != null)
                {
                    m_TMPTextComponent.text = m_ButtonText;
                }
                if (m_ButtonText == value) return;
                InvokePropertyChanged(this, new PropertyChangedEventArgs(nameof(Text)));
            }
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
        }
#endif
    }
}