﻿using UnityEditor;
using UnityEditor.UI;

namespace MVVMUI.Elements.Editor
{
    [CustomEditor(typeof(TMPButton))]
    public class TMPButtonEditor : TextButtonEditor
    {
        protected override void OnEnable()
        {
            base.OnEnable();
            m_TextComponentProperty = serializedObject.FindProperty("m_TMPTextComponent");
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}