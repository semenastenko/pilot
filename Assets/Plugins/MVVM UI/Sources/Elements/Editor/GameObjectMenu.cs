using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace MVVMUI.Elements.Editor
{
    public class GameObjectMenu
    {
        public const string MAIN_PATH = "ElementsResources";
        public const string textButtonName = "TextButton.prefab";
        public const string tmpButtonName = "TMPButton.prefab";
        public const string labelName = "Label.prefab";
        public const string tmpLabelName = "TMPLabel.prefab";
        public const string sliderBarName = "SliderBar.prefab";
        public const string switchToggleName = "SwitchToggle.prefab";

        public static T GetPrefab<T>(string name) where T : Object
        {
            var folders = Directory.GetDirectories(Application.dataPath, MAIN_PATH, SearchOption.AllDirectories);
            if (folders.Length == 0) return null;

            var folder = folders[0];
            folder = folder.Remove(0, folders[0].IndexOf("Assets"));

            var element = AssetDatabase.LoadAssetAtPath<T>(folder + "/" + name);
            return element;
        }

        [MenuItem("GameObject/UI/MVVM/TextButton", false, 10)]
        public static void CreateTextButton(MenuCommand menuCommand)
        {
            // Create a custom game object
            var prefab = GetPrefab<GameObject>(textButtonName);
            Create(prefab, menuCommand.context);
        }

        [MenuItem("GameObject/UI/MVVM/TMPButton", false, 10)]
        public static void CreateTMPButton(MenuCommand menuCommand)
        {
            // Create a custom game object
            var prefab = GetPrefab<GameObject>(tmpButtonName);
            Create(prefab, menuCommand.context);
        }

        [MenuItem("GameObject/UI/MVVM/Label", false, 10)]
        public static void CreateTextLabel(MenuCommand menuCommand)
        {
            // Create a custom game object
            var prefab = GetPrefab<GameObject>(labelName);

            Create(prefab, menuCommand.context);
        }

        [MenuItem("GameObject/UI/MVVM/TMPLabel", false, 10)]
        public static void CreateTMPLabel(MenuCommand menuCommand)
        {
            // Create a custom game object
            var prefab = GetPrefab<GameObject>(tmpLabelName);

            Create(prefab, menuCommand.context);
        }

        [MenuItem("GameObject/UI/MVVM/SliderBar", false, 10)]
        public static void CreateSliderBar(MenuCommand menuCommand)
        {
            // Create a custom game object
            var prefab = GetPrefab<GameObject>(sliderBarName);

            Create(prefab, menuCommand.context);
        }

        [MenuItem("GameObject/UI/MVVM/SwitchToggle", false, 10)]
        public static void CreateSwitchToggle(MenuCommand menuCommand)
        {
            // Create a custom game object
            var prefab = GetPrefab<GameObject>(switchToggleName);

            Create(prefab, menuCommand.context);
        }

        private static void Create(GameObject prefab, Object context)
        {
            GameObject go = Object.Instantiate(prefab).gameObject;
            go.name = prefab.name;
            // Ensure it gets reparented if this was a context click (otherwise does nothing)
            GameObjectUtility.SetParentAndAlign(go, context as GameObject);
            // Register the creation in the undo system
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }
    }
}
