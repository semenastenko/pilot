﻿using UnityEditor;
using UnityEditor.UI;

namespace MVVMUI.Elements.Editor
{
    [CustomEditor(typeof(SwitchToggle))]
    public class SwitchToggleEditor : SliderEditor
    {
        protected SerializedProperty m_IsOnProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_IsOnProperty = serializedObject.FindProperty("m_isOn");
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            serializedObject.Update();
            EditorGUILayout.PropertyField(m_IsOnProperty);
            serializedObject.ApplyModifiedProperties();
        }
    }
}