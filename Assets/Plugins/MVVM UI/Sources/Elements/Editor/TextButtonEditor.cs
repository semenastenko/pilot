﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

namespace MVVMUI.Elements.Editor
{
    [CustomEditor(typeof(TextButton))]
    public class TextButtonEditor : ButtonEditor
    {
        protected SerializedProperty m_TextComponentProperty;
        protected SerializedProperty m_ButtonTextProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_TextComponentProperty = serializedObject.FindProperty("m_TextComponent");
            m_ButtonTextProperty = serializedObject.FindProperty("m_ButtonText");
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            serializedObject.Update();
            EditorGUILayout.PropertyField(m_TextComponentProperty);
            EditorGUILayout.PropertyField(m_ButtonTextProperty);
            serializedObject.ApplyModifiedProperties();
        }
    }
}