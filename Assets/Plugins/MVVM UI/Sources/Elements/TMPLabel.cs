﻿using System.Collections.Generic;
using System.ComponentModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MVVMUI.Elements
{
    [AddComponentMenu("UI/MVVM/TMPLable")]
    public class TMPLabel : TextMeshProUGUI, IBindableObject
    {
        public const string TextProperty = "Text";

        public event PropertyChangedEventHandler PropertyChanged;

        public override string text
        {
            get => base.text;
            set
            {
                base.text = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(text)));
            }
        }

        private void SetText(object arg)
        {
            var value = arg as string;
            if (value == null) return;
            text = value;
        }

        private object GetValue()
        => text;

        public Dictionary<string, BindableProperty> GetBindableProperties()
        {
            var properties = new Dictionary<string, BindableProperty>();
            properties.Add(TextProperty, new BindableProperty(SetText, GetValue));
            return properties;
        }
    }
}