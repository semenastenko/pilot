﻿using MvvmHelpers.Commands;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MVVMUI.Elements
{
    [AddComponentMenu("UI/MVVM/SwitchToggle")]
    public class SwitchToggle : Slider, IBindableObject
    {
        public const string CommandProperty = "Command";
        public const string IsOnProperty = "IsOn";
        [SerializeField] protected bool m_isOn;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void InvokePropertyChanged(object sender, PropertyChangedEventArgs e)
        => PropertyChanged?.Invoke(sender, e);

        //public bool IsOn => m_isOn;

        public virtual bool IsOn
        {
            get => m_isOn;
            set
            {
                if (m_isOn == value) return;
                m_isOn = value;
                base.value = m_isOn ? 1f : 0f;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.value)));
            }
        }

        private void SetValue(object arg)
        {
            try
            {
                IsOn = System.Convert.ToBoolean(arg);
            }
            catch
            {
                return;
            }
        }

        private object GetValue()
        => IsOn;

        protected void OnClick(object obj)
        {
            IsOn = !m_isOn;
            if (obj is AsyncCommand<bool>)
            {
                onValueChanged.AddListener(async (x) =>
                {
                    interactable = false;
                    await ((AsyncCommand<bool>)obj).ExecuteAsync(m_isOn);
                    interactable = true;
                });
            }
            else if (obj is Command<bool>)
            {
                onValueChanged.AddListener((x) => ((Command<bool>)obj).Execute(m_isOn));
            }
            else if (obj is AsyncCommand)
            {
                onValueChanged.AddListener(async (x) =>
                {
                    interactable = false;
                    await ((AsyncCommand)obj).ExecuteAsync();
                    interactable = true;
                });
            }
            else if (obj is Command)
            {
                onValueChanged.AddListener((x) => ((Command)obj).Execute(null));
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            //base.OnPointerDown(eventData);
            IsOn = !m_isOn;
        }

        public override void OnDrag(PointerEventData eventData)
        {
            //base.OnDrag(eventData);
        }

        public override void OnMove(AxisEventData eventData)
        {
            //base.OnMove(eventData);
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();
            value = m_isOn ? 1f : 0f;
        }
#endif
        public Dictionary<string, BindableProperty> GetBindableProperties()
        {
            var properties = new Dictionary<string, BindableProperty>();
            properties.Add(IsOnProperty, new BindableProperty(SetValue, GetValue));
            properties.Add(CommandProperty, new BindableProperty(OnClick, null));
            return properties;
        }
    }
}