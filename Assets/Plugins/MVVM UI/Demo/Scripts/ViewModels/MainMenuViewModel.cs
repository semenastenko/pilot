using MvvmHelpers;
using MvvmHelpers.Commands;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace MVVMUI.Demo
{
    public class MainMenuViewModel : ObservableObject
    {
        public AsyncCommand StartCommand { get; }

        string lableText;
        public string LableText
        {
            get => lableText;
            set => SetProperty(ref lableText, value);
        }

        float sliderValue;
        public float SliderValue
        {
            get => sliderValue;
            set => SetProperty(ref sliderValue, value);
        }

        public MainMenuViewModel()
        {
            LableText = "GAME";
            SliderValue = 0;
            StartCommand = new AsyncCommand(StartGame);

            Timer();
        }

        private async void Timer()
        {
            int count = 0;
            while (Application.isPlaying)
            {
                await Task.Delay(1000);
                //LableText = $"GAME {count}";

                SliderValue = count / 10f;
                count++;
            }
        }

        private async Task StartGame()
        {
            LableText = "Start begin in 3";
            await Task.Delay(500);
            LableText = "Start begin in 2";
            await Task.Delay(500);
            LableText = "Start begin in 1";
            await Task.Delay(500);
            LableText = "Start";
            await Task.Delay(500);
            await UIController.Current.GoToPanelAsync(typeof(GameplayPanel), (nameof(GameplayPanel.Title), "Game is ready!".ToString()));
        }
    }
}
