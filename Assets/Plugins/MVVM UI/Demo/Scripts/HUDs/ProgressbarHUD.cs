﻿using System;

namespace MVVMUI.Demo
{
    public class ProgressbarHUD : UIHUD
    {
        public override Type GetBindingContextType()
        => typeof(GameplayHUD);
    }
}
