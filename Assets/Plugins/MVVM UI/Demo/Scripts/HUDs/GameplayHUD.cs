using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MVVMUI.Demo
{
    public class GameplayHUD : UIHUD
    {
        public override Type GetBindingContextType()
        => typeof(GameplayHUD);
    }
}
