﻿using System;

namespace MVVMUI.Demo
{
    public class SettingsPanel : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(SettingsPanel);
    }
}