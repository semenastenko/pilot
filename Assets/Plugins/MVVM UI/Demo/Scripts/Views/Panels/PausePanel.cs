﻿using System;

namespace MVVMUI.Demo
{
    public class PausePanel : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(PausePanel);
    }
}