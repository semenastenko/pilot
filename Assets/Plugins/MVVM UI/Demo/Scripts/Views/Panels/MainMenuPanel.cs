using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MVVMUI.Demo
{
    public class MainMenuPanel : UIPanel
    {
        public string Title { get; set; }
        public override Type GetBindingContextType()
        => typeof(MainMenuViewModel);

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            Debug.Log($"Title {Title}");
        }
    }
}