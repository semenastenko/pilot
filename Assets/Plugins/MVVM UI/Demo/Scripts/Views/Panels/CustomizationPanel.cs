﻿using System;

namespace MVVMUI.Demo
{
    public class CustomizationPanel : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(CustomizationPanel);
    }
}