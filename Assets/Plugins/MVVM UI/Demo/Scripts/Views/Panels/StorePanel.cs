﻿using System;

namespace MVVMUI.Demo
{
    public class StorePanel : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(StorePanel);
    }
}