﻿using System;

namespace MVVMUI.Demo
{
    public class GameoverPanel : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(GameoverPanel);
    }
}