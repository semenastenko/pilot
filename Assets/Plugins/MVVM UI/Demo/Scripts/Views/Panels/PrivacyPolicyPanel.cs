﻿using System;

namespace MVVMUI.Demo
{
    public class PrivacyPolicyPanel : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(PrivacyPolicyPanel);
    }
}