﻿using MvvmHelpers.Commands;
using System;
using System.Threading.Tasks;
using UnityEngine;

namespace MVVMUI.Demo
{
    public class GameplayPanel : UIPanel
    {
        private string title;
        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }

        public Command DisplayDialogCommang { get; private set; }
        public Command ShowMessageCommang { get; private set; }

        public override Type GetBindingContextType()
        => typeof(GameplayPanel);

        public override void OnInitialize()
        {
            DisplayDialogCommang = new Command(OnDisplayDialog);
            ShowMessageCommang = new Command(OnMessageDialog);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            UIController.Current.AddHUD<GameplayHUD>();
            UIController.Current.AddHUD<ProgressbarHUD>();
        }

        protected override void OnDisappearing()
        {
            UIController.Current.RemoveAllHUD();
        }

        private async void OnDisplayDialog(object obj)
        {
            var result = await UIController.Current.DisplayPopup(typeof(DialogPopup), (nameof(DialogPopup.Message), "This dialog from Gameplay!"));

            Title = $"dialog result: {result}";
        }

        private async void OnMessageDialog(object obj)
        {
            var result = await UIController.Current.DisplayPopup(typeof(MessagePopup), (nameof(MessagePopup.Message), "This message from Gameplay!"));

            Title = $"message result: {result}";
        }
    }
}