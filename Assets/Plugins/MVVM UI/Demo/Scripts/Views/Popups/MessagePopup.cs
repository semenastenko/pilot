﻿using MvvmHelpers.Commands;
using System;

namespace MVVMUI.Demo
{
    public class MessagePopup : UIPopup
    {
        string message;

        public string Message
        {
            get => message;
            set => SetProperty(ref message, value);
        }

        public Command OkCommand { get; private set; }

        public override Type GetBindingContextType()
        => typeof(MessagePopup);

        public override void OnInitialize()
        {
            OkCommand = new Command(OnOk);
        }

        private void OnOk()
        {
            SetResult(true);
        }
    }
}