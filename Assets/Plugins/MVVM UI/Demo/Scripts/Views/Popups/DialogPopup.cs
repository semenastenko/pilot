using MvvmHelpers.Commands;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace MVVMUI.Demo
{
    public class DialogPopup : UIPopup
    {
        string message;

        public string Message
        {
            get => message;
            set => SetProperty(ref message, value);
        }

        public Command YesCommand { get; private set; }
        public Command NoCommand { get; private set; }

        public override Type GetBindingContextType()
        => typeof(DialogPopup);

        public override void OnInitialize()
        {
            YesCommand = new Command(OnYes);
            NoCommand = new Command(OnNo);
        }

        private void OnNo()
        {
            SetResult(false);
        }

        private void OnYes()
        {
            SetResult(true);
        }
    }
}