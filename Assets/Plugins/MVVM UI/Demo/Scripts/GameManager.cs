using MVVMUI.Elements;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MVVMUI.Demo
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] Canvas canvas;
        private void Awake()
        {
            var navigation = new UIController(canvas);
            UIController.Current.SetActive(typeof(MainMenuPanel), (nameof(MainMenuPanel.Title), "MainMenuPanel.Title".ToString()));
        }
    }
}
