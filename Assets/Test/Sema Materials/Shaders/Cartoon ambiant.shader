// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Cartoons/Cartoon ambiant"
{
	Properties
	{
		_Color("Tint Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal/Bump Map", 2D) = "bump" {}
		_MetallicGlossMap("Metallic map", 2D) = "black" {}

		[Header(Lightning)]
		[Space(10)]
		[HDR] _AmbientColor("Ambient Color", Color) = (0,0,0,1)
		[HDR] _SpecularColor("Specular Color", Color) = (0,0,0,1)
		_Glossiness("Glossiness", Float) = 32
		[HDR] _RimColor("Rim Color", Color) = (1,1,1,1)
		_RimAmount("Rim Amount", Range(0, 1)) = 0.716
		_RimThreshold("Rim Threshold", Range(0, 1)) = 0.1

		[Header(CurveHorizon)]
		[Space(10)]
		_CurveIntensity("Intensity Curving", Float) = 0.001
	}

		SubShader
		{
			Tags { "RenderType" = "Opaque" }

			LOD 200

			CGPROGRAM
			#pragma surface surf Cel vertex:vert 
			#include "UnityCG.cginc"

			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			#pragma target 3.0

			sampler2D _MainTex;
			sampler2D _BumpMap;
			sampler2D _MetallicGlossMap;

			
			//sampler2D _DecalTex;
			//float4 _DecalColor;
			float4 _Color;
			float4 _AmbientColor;
			float4 _SpecularColor;
			float4 _RimColor;
			float _Glossiness;
			float _RimAmount;
			float _RimThreshold;
			float _CurveIntensity;
			//float _HeightShift;

			float4 LightingCel(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
			{
				float NdotL = dot(_WorldSpaceLightPos0, s.Normal);

				float3 halfVector = normalize(_WorldSpaceLightPos0 + viewDir);

				float NdotH = dot(s.Normal, halfVector);

				float specularIntensity = pow(NdotH, _Glossiness * _Glossiness);

				float specularIntensitySmooth = smoothstep(0.005h, 0.01h, specularIntensity);

				float4 specular = specularIntensitySmooth * _SpecularColor;

				float rimDot = (1.0h - dot(s.Normal, viewDir)) * pow(NdotL, 0.3h);
				float fresnelSize = 1.0h - _RimAmount;

				float4 rim = smoothstep(fresnelSize, fresnelSize * 1.1h, rimDot) * _RimColor;

				return float4(s.Albedo.rgb, 1.0h) * (_LightColor0 + _AmbientColor + specular + rim) * _Color;
			}

			struct Input
			{
				float4 pos : SV_POSITION;
				//float2 uv_MainTex;
				float3 worldPos;
				float3 normal;
				float2 uv_BumpMap;
				//float2 uv_DecalTex;
			};

			

			void vert(inout appdata_full v, out Input o)
			{
				UNITY_INITIALIZE_OUTPUT(Input, o);


				float3 world = mul(unity_ObjectToWorld, v.vertex);
				world.xyz -= _WorldSpaceCameraPos;
				world = float4(0.0f, (world.z * world.z) * -_CurveIntensity, 0.0f, 0.0f);
				v.vertex += mul(unity_ObjectToWorld, world);

				o.pos = UnityObjectToClipPos(v.vertex);
				//calculate world position of vertex
				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.worldPos = worldPos.xyz;
				//calculate world normal
				float3 worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
				o.normal = normalize(worldNormal);
			}
			float4 _MainTex_ST;

			void surf(Input IN, inout SurfaceOutput o)
			{
				float2 uv_front = TRANSFORM_TEX(IN.worldPos.xy, _MainTex);
				float2 uv_side  = TRANSFORM_TEX(IN.worldPos.zy, _MainTex);
				float2 uv_top   = TRANSFORM_TEX(IN.worldPos.xz, _MainTex);

				//read texture at uv position of the three projections
				fixed4 col_front = tex2D(_MainTex, uv_front);
				fixed4 col_side = tex2D(_MainTex, uv_side);
				fixed4 col_top = tex2D(_MainTex, uv_top);

				//generate weights from world normals
				float3 weights = IN.normal;
				//show texture on both sides of the object (positive and negative)
				weights = abs(weights);
				//make the transition sharper
				weights = pow(weights, 1);
				//make it so the sum of all components is 1
				weights = weights / (weights.x + weights.y + weights.z);

				//combine weights with projected colors
				col_front *= weights.z;
				col_side *= weights.x;
				col_top *= weights.y;

				//combine the projected colors
				fixed4 col = col_front + col_side + col_top;

				o.Albedo = col.rgb;
				o.Alpha = col.a;
				o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			}

			ENDCG
		}

			FallBack off
}
