using Cysharp.Threading.Tasks;
using Kudos.Events;
using Kudos.Input;
using Kudos.SimpleGeneration;
using System;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kudos.Player
{
    public class PlayerPlane : MonoBehaviour, IDisposable, IGameEventHandler
    {
        [Inject] ScreenControl _screenControl;
        [Inject] PlayerSettings _settings;
        [Inject] CameraFollower _cameraFollower;
        public ChunkPlacer ChunkPlacer;
        private Movement _movement;
        private Accelerating _accelerator;
        IDisposable fixedMoveDispose;
        IDisposable moveDispose;
        private bool isCollision;
        private Vector3 startPos;
        [SerializeField] ParticleSystem explosionParticle;
        [SerializeField] ParticleSystem electricParticle;
        [SerializeField] ParticleSystem puffParticle;
        [SerializeField] PlayerRagdoll mainRagdoll;
        [SerializeField] PlayerRagdoll ragdoll;
        [SerializeField, Range(-1, 1)] private float pitch = 0f;
        [SerializeField, Range(-1, 1)] private float yaw = 0f;
        [SerializeField, Range(-1, 1)] private float roll = 0f;

        public Movement Movement => _movement;
        public Vector3 Velocity { get; private set; }

        public float Speed { get; private set; }

        //public event Action OnRestart;
        public event Action<IGameEvent> OnEvent;

        private void Awake()
        {
            _movement = new RelativeMovement(GetComponent<Rigidbody>(), _settings);
            _accelerator = new Accelerating(_settings);
            startPos = transform.position;
            _cameraFollower.SetSettings(mainRagdoll.Pelvis, _settings.LookAtOffset, transform, _settings.CameraOffset, _settings.CameraSmoothTime);
            StartParams();
        }

        public void StartParams()
        {
            transform.position = startPos;
            transform.rotation = Quaternion.identity;
            _screenControl.Reset();
            _accelerator.Reset();
            _movement?.Dispose();
            ragdoll.gameObject.SetActive(false);
            mainRagdoll.gameObject.SetActive(true);
            ragdoll.SetSleep(false);
            ragdoll.SetTransforms(mainRagdoll);
            _cameraFollower.SetSettings(mainRagdoll.Pelvis, _settings.LookAtOffset, transform, _settings.CameraOffset, _settings.CameraSmoothTime);
            Speed = 1;
        }

        public void StartMoving()
        {
            isCollision = false;
            fixedMoveDispose = Observable.EveryFixedUpdate()
                .Subscribe(_ => FixedMoving());
            moveDispose = Observable.EveryUpdate()
                .Subscribe(_ => Moving());
        }

        private void FixedMoving()
        {
            Velocity = Vector3.zero;
            if (isCollision) return;
            var inputDirection = _screenControl.InputZoneDelta;
            var acc = _accelerator.Accelerate(new Vector3(Speed, 0));

            //Debug.Log($"Accelerator {acc}");
            Velocity = _movement.FixedMove(new Vector3(inputDirection.x, inputDirection.y, _screenControl.TapDirection), acc.x);
        }

        private void Moving()
        {
            Velocity = Vector3.zero;
            if (isCollision) return;
            var inputDirection = _screenControl.InputZoneDelta;
            Velocity = _movement.Move(new Vector3(inputDirection.x, inputDirection.y, _screenControl.TapDirection));
            if (_movement is RelativeMovement)
            {
                pitch = (_movement as RelativeMovement).Pitch;
                yaw = (_movement as RelativeMovement).Yaw;
                roll = (_movement as RelativeMovement).Roll;
            }
        }

        public void IncreaseSpeed()
        {
            puffParticle.Play();
            Speed += _settings.IncreaseSpeed;
        }

        public void DecreaseSpeed()
        {
            electricParticle.Play();
            _accelerator.Reset();
            //if (Speed > 0.75f)
            //    Speed -= _settings.IncreaseSpeed;
        }

        public async void Die()
        {
            if (isCollision) return;
            isCollision = true;
            //explosionParticle?.Play();
            mainRagdoll.gameObject.SetActive(false);
            ragdoll.gameObject.SetActive(true);
            _cameraFollower.SetSettings(ragdoll.Pelvis, _settings.LookAtOffset, transform, _settings.CameraOffset, _settings.CameraSmoothTime);
            OnEvent?.Invoke(new GameStateEvent(GameState.end));
            await UniTask.Delay(1000);
            OnEvent?.Invoke(new EndGameStateEvent(WinState.lose));
        }

        private void OnCollisionEnter(Collision collision)
        {
            Die();
        }

        private void OnCollisionExit(Collision collision)
        {
            isCollision = false;
        }

        public void Dispose()
        {
            fixedMoveDispose?.Dispose();
            moveDispose?.Dispose();
            _movement?.Dispose();
        }

        public async void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is GameStateEvent)
            {
                var state = (GameStateEvent)gameEvent;
                if (state.State == GameState.begin)
                {

                    Debug.Log($"StartMoving {name}");
                    SeedRandom.RandomizeSeed();
                    await ChunkPlacer.WaitLoadingChunks();
                    StartMoving();
                }
                else if (state.State == GameState.end)
                {
                    Dispose();
                }
                else if (state.State == GameState.reset)
                {
                    StartParams();
                    _cameraFollower.ResetPos();
                }
            }
        }

        public int GetPriority() => 1;
    }
}