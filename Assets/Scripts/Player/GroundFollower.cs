using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Kudos.Player
{
    public class GroundFollower : MonoBehaviour
    {
        [Inject] PlayerPlane target;

        private void FixedUpdate()
        {
            transform.position = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
        }
    }
}
