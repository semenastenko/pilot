using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Kudos.Player
{
    public class Movement : IDisposable
    {
        protected PlayerSettings _settings;
        protected Rigidbody _rigidbody;

        private Camera _camera;

        protected Vector3 oldPosition;
        private Vector3 remainingTranslation;

        public Movement(Rigidbody rigidbody, PlayerSettings settings)
        {
            _rigidbody = rigidbody;
            _settings = settings;
            _camera = Camera.main;
        }

        public virtual Vector3 Move(Vector3 deltaAxis)
        {
            Vector3 newPosition = _rigidbody.position;
            oldPosition = _rigidbody.position;

            var screenDelta = -new Vector2(deltaAxis.x, deltaAxis.y);
            screenDelta.y = -screenDelta.y;

            if (screenDelta != Vector2.zero)
            {
                newPosition = Translate(screenDelta * _settings.TurnTorque);
            }
            newPosition += Vector3.forward * _settings.Thrust * (5 - deltaAxis.magnitude) * Time.deltaTime;

            // Increment
            remainingTranslation += newPosition - oldPosition;

            // Get t value
            var factor = GetDampenFactor(_settings.Damping, Time.deltaTime);

            // Dampen remainingDelta
            var newRemainingTranslation = Vector3.Lerp(remainingTranslation, Vector3.zero, factor);

            // Shift this transform by the change in delta
            Vector3 pos = oldPosition + remainingTranslation - newRemainingTranslation;
            _rigidbody.position = ClampPosition(new Vector3(pos.x, pos.y, pos.z));

            if (screenDelta == Vector2.zero && _settings.Inertia > 0.0f && _settings.Damping > 0.0f)
            {
                newRemainingTranslation = Vector3.Lerp(newRemainingTranslation, remainingTranslation, _settings.Inertia);
            }

            // Update remainingDelta with the dampened value
            remainingTranslation = newRemainingTranslation;

            return -(_rigidbody.position - oldPosition);
        }

        public virtual Vector3 FixedMove(Vector3 axis, float speed)
        {
            return -(_rigidbody.position - oldPosition);
        }

        private Vector3 Translate(Vector2 screenDelta)
        {
            if (_camera != null)
            {
                // Screen position of the transform
                var screenPoint = _camera.WorldToScreenPoint(_rigidbody.position);

                // Add the deltaPosition
                screenPoint += (Vector3)screenDelta;

                // Convert back to world space
                Vector3 pos = _camera.ScreenToWorldPoint(screenPoint);
                //_rb.position = ClampPosition(new Vector3(pos.x, pos.y, pos.z));
                return ClampPosition(new Vector3(pos.x, pos.y, pos.z));
            }

            return Vector3.zero;
        }

        private Vector3 ClampPosition(Vector3 position)
        {
            if (_camera != null)
            {
                var screenPoint = _camera.WorldToScreenPoint(position);

                screenPoint = new Vector3(Mathf.Clamp(screenPoint.x, 0, Screen.width),
                    Mathf.Clamp(screenPoint.y, 0, Screen.height),
                    screenPoint.z);

                return _camera.ScreenToWorldPoint(screenPoint);
            }

            return position;
        }

        private float GetDampenFactor(float damping, float elapsed)
        {
            if (damping < 0.0f)
            {
                return 1.0f;
            }

            return 1.0f - Mathf.Exp(-damping * elapsed);
        }

        public virtual void Dispose()
        {
            oldPosition = Vector3.zero;
            remainingTranslation = Vector3.zero;
        }
    }

    public class Smoothing
    {
        private readonly float _smoothTime;

        private Vector3 _currentSmooth;
        private Vector3 _velocity;

        public Smoothing(float smoothTime)
        {
            _smoothTime = smoothTime;
        }

        public Vector3 GetSmoothFactor(Vector3 target)
        {
            _currentSmooth = Vector3.SmoothDamp(_currentSmooth, target, ref _velocity, _smoothTime * Time.deltaTime);
            return _currentSmooth;
        }

        public Vector3 GetAccelerationFactor(Vector3 target, float pow) =>
            new Vector3(Mathf.Pow(target.x, pow), Mathf.Pow(target.y, pow), Mathf.Pow(target.z, pow));

        public void Reset()
        {
            _currentSmooth = Vector3.zero;
            _velocity = Vector3.zero;
        }
    }

    public class Accelerating
    {
        private readonly PlayerSettings _settings;

        private Vector3 _currentAccelerateVector;

        public Accelerating(PlayerSettings settings)
        {
            _settings = settings;
        }

        public Vector3 Accelerate(Vector3 target)
        {
            var accelerateX = Mathf.Pow(target.x, _settings.Acceleration) + 1;
            var accelerateY = Mathf.Pow(target.y, _settings.Acceleration) + 1;

            var deltaX = Mathf.Lerp(_currentAccelerateVector.x, target.x * _settings.ForceMult, accelerateX * Time.deltaTime);
            var deltaY = Mathf.Lerp(_currentAccelerateVector.y, target.y * _settings.ForceMult, accelerateY * Time.deltaTime);
            _currentAccelerateVector = new Vector3(deltaX, deltaY);
            return _currentAccelerateVector;
        }

        public void Reset()
        {
            _currentAccelerateVector = Vector3.zero;
        }
    }
}
