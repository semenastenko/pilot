using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Kudos.Player
{
    public class CameraFollower : MonoBehaviour
    {
        private Vector3 velocity;
        private Vector3 startPos;

        public Transform LookAtTarget { get; private set; }
        public Transform Target { get; private set; }
        public Vector3 CameraOffset { get; private set; }
        public Vector3 LookAtOffset { get; private set; }
        public float CameraSmoothTime { get; private set; }

        private bool isLerp = false;
        private void Start()
        {
            startPos = transform.position;
        }

        public void SetSettings(Transform lookAtTarget, Vector3 lookAtOffset, Transform target = null, Vector3? cameraOffset = null, float? cameraSmoothTime = null)
        {
            LookAtTarget = lookAtTarget;
            Target = target;
            LookAtOffset = lookAtOffset;
            if (cameraOffset.HasValue)
                CameraOffset = cameraOffset.Value;
            if (cameraSmoothTime.HasValue)
                CameraSmoothTime = cameraSmoothTime.Value;
        }

        public async UniTask LerpTarget(Transform newTarget, float time)
        {
            isLerp = true;
            await transform.DOMove(newTarget.position + CameraOffset, time).AsyncWaitForCompletion();
            Target = newTarget;
            isLerp = false;
        }

        private void FixedUpdate()
        {
            if (Target != null && !isLerp)
                transform.position = Vector3.SmoothDamp(transform.position, Target.position + CameraOffset, ref velocity, CameraSmoothTime * Time.deltaTime);
            if (LookAtTarget != null)
                transform.LookAt(LookAtTarget.position + LookAtOffset);
        }

        public void ResetPos()
        {
            transform.position = startPos;
        }
    }
}
