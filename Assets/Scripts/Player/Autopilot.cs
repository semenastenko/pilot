using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Kudos.Player
{
    public class Autopilot : IDisposable
    {
        private PlayerSettings _settings;
        public Vector3 TargetPos { get; private set; }

        public Autopilot(PlayerSettings settings)
        {
            _settings = settings;
            TargetPos = Vector3.forward + Vector3.up * 2;
        }

        public void UpdateTargetPos(Vector3 pos)
        {
            TargetPos = pos * _settings.AimDistance;
        }

        public void RunAutopilot(Transform transform, out float yaw, out float pitch, out float roll)
        {
            var sensetivityMultiply = _settings.ControlSensitivity;
            if (UnityEngine.Input.touchCount == 0)
            {
                sensetivityMultiply = _settings.Sensitivity;
            }
            var localFlyTarget = transform.InverseTransformPoint(TargetPos).normalized * sensetivityMultiply;
            var angleOffTarget = Vector3.Angle(transform.forward, TargetPos - transform.position);

            yaw = Mathf.Clamp(localFlyTarget.x, -1f, 1f);
            pitch = -Mathf.Clamp(localFlyTarget.y, -1f, 1f);

            var agressiveRoll = Mathf.Clamp(localFlyTarget.x, -1f, 1f);

            var wingsLevelRoll = transform.right.y;

            var wingsLevelInfluence = Mathf.InverseLerp(0f, _settings.AggressiveTurnAngle, angleOffTarget);
            roll = Mathf.Lerp(wingsLevelRoll, agressiveRoll, wingsLevelInfluence);
        }

        public void Dispose()
        {
            TargetPos = Vector3.forward + Vector3.up * 2;
        }
    }
}
