﻿using System;
using UnityEngine;

namespace Kudos.Player
{
    public class RagdollCollisionDetect : MonoBehaviour
    {
        public event Action<Collider> OnCollision;
        private void OnCollisionEnter(Collision collision)
        {
            OnCollision?.Invoke(collision.collider);
        }
    }
}