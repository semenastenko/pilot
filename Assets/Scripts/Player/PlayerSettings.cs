using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.Player
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "SO/Plane/PlayerSettings")]
    public class PlayerSettings : ScriptableObject
    {
        [Header("Plane Physics")]
        [SerializeField, Tooltip("Force to push plane forwards with")] float thrust;
        [SerializeField, Tooltip("Pitch, Yaw, Roll")] Vector3 turnTorque;
        [SerializeField, Tooltip("Multiplier for all forces")] float forceMult;
        [SerializeField] float damping;
        [SerializeField] float inertia;
        [SerializeField] float acceleration;
        [SerializeField] float increaseSpeed;
        [Header("Autopilot")]
        [SerializeField, Tooltip("Sensitivity for autopilot flight.")] float sensitivity;
        [SerializeField, Tooltip("Control sensitivity for autopilot flight.")] float controlSensitivity;
        [SerializeField, Tooltip("Angle at which airplane banks fully into target.")] float aggressiveTurnAngle;
        [SerializeField, Tooltip("How far the boresight and mouse flight are from the aircraft")] float aimDistance;
        [Header("Camera Settings")]
        [SerializeField] float cameraSmoothTime;
        [SerializeField] Vector3 cameraOffset;
        [SerializeField] Vector3 lookAtOffset;
        [Header("Control Settings")]
        [SerializeField, Range(0, 2)] float zoneRange;
        [Header("Bool parameters")]
        [SerializeField] bool verticalInverse;

        public float Thrust => thrust;
        public Vector3 TurnTorque => turnTorque;
        public float Damping => damping;
        public float Inertia => inertia;
        public float ForceMult => forceMult;
        public float Acceleration => acceleration;
        public float IncreaseSpeed => increaseSpeed;
        public float Sensitivity => sensitivity;
        public float ControlSensitivity => controlSensitivity;
        public float AggressiveTurnAngle => aggressiveTurnAngle;
        public float AimDistance => aimDistance;
        public float CameraSmoothTime => cameraSmoothTime;
        public Vector3 CameraOffset => cameraOffset;
        public Vector3 LookAtOffset => lookAtOffset;
        public float ZoneRange => zoneRange;
        public bool VerticalInverse => verticalInverse;

        public void SetVerticalInverse(bool value) => verticalInverse = value;
    }
}
