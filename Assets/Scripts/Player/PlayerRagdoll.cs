﻿using Kudos.SimpleGeneration;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.Player
{
    public class PlayerRagdoll : MonoBehaviour
    {
        [SerializeField] Transform pelvis; public Transform Pelvis => pelvis;
        [SerializeField] Transform leftHips; public Transform LeftHips => leftHips;
        [SerializeField] Transform leftKnee; public Transform LeftKnee => leftKnee;
        [SerializeField] Transform leftFoot; public Transform LeftFoot => leftFoot;
        [SerializeField] Transform rightHips; public Transform RightHips => rightHips;
        [SerializeField] Transform rightKnee; public Transform RightKnee => rightKnee;
        [SerializeField] Transform rightFoot; public Transform RightFoot => rightFoot;
        [SerializeField] Transform leftArm; public Transform LeftArm => leftArm;
        [SerializeField] Transform leftElbow; public Transform LeftElbow => leftElbow;
        [SerializeField] Transform rightArm; public Transform RightArm => rightArm;
        [SerializeField] Transform rightElbow; public Transform RightElbow => rightElbow;
        [SerializeField] Transform middleSpine; public Transform MiddleSpine => middleSpine;
        [SerializeField] Transform head; public Transform Head => head;

        List<Rigidbody> rigidbodies = new List<Rigidbody>();
        List<RagdollCollisionDetect> collisionDetects = new List<RagdollCollisionDetect>();

        private void Awake()
        {
            var transforms = new Transform[] { pelvis, leftHips, leftKnee, leftFoot, rightHips, rightKnee, rightFoot,
            leftArm, leftElbow, rightArm, rightElbow, middleSpine, head};
            foreach (var item in transforms)
            {
                if (item.GetComponent<Rigidbody>() is var rb && rb != null)
                {
                    rigidbodies.Add(rb);
                }
                if (item.GetComponent<RagdollCollisionDetect>() is var cd && cd != null)
                {
                    collisionDetects.Add(cd);
                }
            }

            foreach (var cd in collisionDetects)
            {
                cd.OnCollision += OnCollision;
            }
        }

        private void OnCollision(Collider obj)
        {
            var chunk = obj.GetComponent<Chunk>();
            if (chunk != null)
                SetSleep(true);
        }

        public void SetTransforms(PlayerRagdoll ragdoll)
        {
            pelvis.localPosition = ragdoll.pelvis.localPosition;
            pelvis.localRotation = ragdoll.pelvis.localRotation;

            leftHips.localPosition = ragdoll.leftHips.localPosition;
            leftHips.localRotation = ragdoll.leftHips.localRotation;

            leftKnee.localPosition = ragdoll.leftKnee.localPosition;
            leftKnee.localRotation = ragdoll.leftKnee.localRotation;

            leftFoot.localPosition = ragdoll.leftFoot.localPosition;
            leftFoot.localRotation = ragdoll.leftFoot.localRotation;

            rightHips.localPosition = ragdoll.rightHips.localPosition;
            rightHips.localRotation = ragdoll.rightHips.localRotation;

            rightKnee.localPosition = ragdoll.rightKnee.localPosition;
            rightKnee.localRotation = ragdoll.rightKnee.localRotation;

            rightFoot.localPosition = ragdoll.rightFoot.localPosition;
            rightFoot.localRotation = ragdoll.rightFoot.localRotation;

            leftArm.localPosition = ragdoll.leftArm.localPosition;
            leftArm.localRotation = ragdoll.leftArm.localRotation;

            leftElbow.localPosition = ragdoll.leftElbow.localPosition;
            leftElbow.localRotation = ragdoll.leftElbow.localRotation;

            rightArm.localPosition = ragdoll.rightArm.localPosition;
            rightArm.localRotation = ragdoll.rightArm.localRotation;

            rightElbow.localPosition = ragdoll.rightElbow.localPosition;
            rightElbow.localRotation = ragdoll.rightElbow.localRotation;

            middleSpine.localPosition = ragdoll.middleSpine.localPosition;
            middleSpine.localRotation = ragdoll.middleSpine.localRotation;

            head.localPosition = ragdoll.head.localPosition;
            head.localRotation = ragdoll.head.localRotation;
        }

        public void SetSleep(bool value)
        {
            foreach (var rb in rigidbodies)
            {
                rb.isKinematic = value;
                rb.Sleep();
            }
        }
    }
}