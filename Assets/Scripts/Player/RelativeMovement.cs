using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Kudos.Input;

namespace Kudos.Player
{
    public class RelativeMovement : Movement
    {
        private Autopilot autopilotTarget;

        private float pitch = 0f;
        private float yaw = 0f;
        private float roll = 0f;

        public float Pitch { set { pitch = Mathf.Clamp(value, -1f, 1f); } get { return pitch; } }
        public float Yaw { set { yaw = Mathf.Clamp(value, -1f, 1f); } get { return yaw; } }
        public float Roll { set { roll = Mathf.Clamp(value, -1f, 1f); } get { return roll; } }

        public RelativeMovement(Rigidbody rigidbody, PlayerSettings settings)
            : base(rigidbody, settings)
        {
            autopilotTarget = new Autopilot(settings);
        }

        public override Vector3 Move(Vector3 axis)
        {
            float autoYaw = 0;
            float autoPitch = 0;
            float autoRoll = 0;

            autopilotTarget.RunAutopilot(_rigidbody.transform, out autoYaw, out autoPitch, out autoRoll);

            yaw = autoYaw;
            pitch = autoPitch;
            roll = autoRoll;

            return -(_rigidbody.position - oldPosition);
        }

        public override Vector3 FixedMove(Vector3 axis, float speed)
        {
            var forwardDir = Mathf.Clamp01((1 - Mathf.Abs(axis.x)) * (1 - Mathf.Abs(axis.y))) - 0.1f;
            Vector3 dir = new Vector3(-axis.x, _settings.VerticalInverse ? axis.y : -axis.y, forwardDir);
            autopilotTarget.UpdateTargetPos(_rigidbody.position + dir);
            //Debug.DrawLine(_rigidbody.position, _rigidbody.position + dir);
            _rigidbody.AddRelativeForce(Vector3.forward * _settings.Thrust * _settings.ForceMult * speed, ForceMode.Force);
            _rigidbody.AddRelativeTorque(new Vector3(_settings.TurnTorque.x * pitch,
                                                _settings.TurnTorque.y * yaw,
                                                -_settings.TurnTorque.z * roll) * _settings.ForceMult,
                                    ForceMode.Acceleration);
            return -(_rigidbody.position - oldPosition);
        }

        public override void Dispose()
        {
            base.Dispose();
            if (_rigidbody != null)
            {
                _rigidbody.velocity = Vector3.zero;
                _rigidbody.angularVelocity = Vector3.zero;
            }
            autopilotTarget?.Dispose();
        }
    }
}
