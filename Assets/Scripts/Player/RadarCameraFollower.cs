using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Kudos.Player
{
    public class RadarCameraFollower : MonoBehaviour
    {
        [Inject] PlayerPlane target;
        private Transform _arrow;
        private Camera _camera;

        [SerializeField] float height = 100;
        [SerializeField] float offsetScale = 0.75f;
        [SerializeField] float arrowScale = 10;

        private void Start()
        {
            _arrow = transform.GetChild(0);
            _camera = GetComponent<Camera>();
        }

        private void FixedUpdate()
        {
            var offset = new Vector3(0, height, _camera.orthographicSize * offsetScale);
            transform.position = target.transform.position + offset;
            _arrow.localScale = Vector3.one * _camera.orthographicSize / arrowScale;
            _arrow.transform.position = target.transform.position + Vector3.up * (height-1);
            var andel = Vector3.SignedAngle(target.transform.forward, Vector3.forward, Vector3.up);

            _arrow.localEulerAngles = Vector3.forward * andel;
        }
    }
}
