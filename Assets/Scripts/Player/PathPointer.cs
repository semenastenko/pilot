﻿using Kudos.Enemy;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Kudos.Player
{
    public class PathPointer : MonoBehaviour
    {
        [SerializeField] float offset;
        [SerializeField] Sprite arrowSprite;
        [SerializeField] Sprite pointSprite;
        [SerializeField] float arrowScale;
        [SerializeField] float pointScale;
        [SerializeField] float arrowAlpha;
        [SerializeField] float pointAlpha;

        [Inject] EnemyMoveControl _enemy;
        private Collider _enemyCollider;
        private Camera _camera;
        private Image _pointer;
        private RectTransform rectTransform;
        private void Awake()
        {
            _camera = Camera.main;
            _enemyCollider = _enemy.GetComponent<Collider>();
            _pointer = transform.GetChild(0).GetComponent<Image>();
            rectTransform = transform as RectTransform;
        }

        void Update()
        {
            var planes = GeometryUtility.CalculateFrustumPlanes(_camera);
            if (GeometryUtility.TestPlanesAABB(planes, _enemyCollider.bounds))
            {
                //_pointer.gameObject.SetActive(false);
                _pointer.sprite = pointSprite;
                _pointer.color = new Color(_pointer.color.r, _pointer.color.g, _pointer.color.b, pointAlpha);
                MovePoint();
            }
            else
            {
                _pointer.sprite = arrowSprite;
                _pointer.color = new Color(_pointer.color.r, _pointer.color.g, _pointer.color.b, arrowAlpha);
                _pointer.gameObject.SetActive(true);
                MoveArrow();
            }
        }

        private void MoveArrow()
        {
            var dir = _enemy.transform.position - _camera.transform.position;
            var horizontalAngle = Vector3.SignedAngle(_camera.transform.right, dir, _camera.transform.forward) - 90;
            //horizontalAngle += horizontalAngle * (1 - 60 / _camera.fieldOfView) - 90;
            var horizontalVector = Quaternion.Euler(0, 0, horizontalAngle) * Vector2.up;
            var horizontalPos = horizontalVector.x * rectTransform.rect.width;

            var verticalAngle = Vector3.SignedAngle(_camera.transform.up, dir, _camera.transform.forward);
            verticalAngle += Mathf.Sign(verticalAngle) * (90 - Mathf.Abs(verticalAngle)) * (1 - 60 / _camera.fieldOfView);
            var verticalVector = Quaternion.Euler(0, 0, verticalAngle) * Vector2.up;
            var verticalPos = verticalVector.y * rectTransform.rect.height;

            var screenPos = new Vector2(
                Mathf.Clamp(horizontalPos, -rectTransform.rect.width / 2 + offset, rectTransform.rect.width / 2 - offset),
                Mathf.Clamp(verticalPos, -rectTransform.rect.height / 2 + offset, rectTransform.rect.height / 2 - offset));
            (_pointer.transform as RectTransform).anchoredPosition = screenPos;
            _pointer.transform.rotation = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.up, screenPos));
            _pointer.transform.localScale = Vector3.one * arrowScale;
            //Debug.Log($"horizontalAngle {horizontalAngle} verticalAngle {verticalAngle}");
            //Debug.Log($"angle {angle}");
        }

        private void MovePoint()
        {
            var dir = _enemy.transform.position - _camera.transform.position;
            var ray = new Ray(_camera.transform.position, dir);
            if (Physics.Raycast(ray, dir.magnitude - 10))
            {
                _pointer.gameObject.SetActive(true);
                var pos = _camera.WorldToScreenPoint(_enemy.transform.position);
                _pointer.transform.position = pos;
                _pointer.transform.rotation = Quaternion.identity;
                _pointer.transform.localScale = Vector3.one * pointScale;
            }
            else
            {
                _pointer.gameObject.SetActive(false);
            }
        }
    }
}
