using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneAnimator
{
    private Animator _animator;
    public Animator Animator => _animator;

    public PlaneAnimator(Animator animator)
    {
        _animator = animator;
    }
    public void Animate(Vector3 axisDirection)
    {
        _animator.SetFloat("AxisHorizontal", -axisDirection.y);
        _animator.SetFloat("AxisVertical", axisDirection.x);
    }

    public void SetActiveAnimation(bool value)
    {
        if (_animator != null && _animator.enabled != value)
        {
            _animator.enabled = value;
        }
    }
}
