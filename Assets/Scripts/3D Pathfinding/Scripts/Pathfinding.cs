using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Kudos.Navigation
{
    public class Pathfinding
    {
        private static Pathfinding instance;
        public static Pathfinding Instance
        {
            get
            {
                if (instance == null)
                    instance = new Pathfinding();
                return instance;
            }
            private set => instance = value;
        }
        public int GridWidth { get; private set; }
        public int GridHeight { get; private set; }
        public int GridLength { get; private set; }
        public float PointDistance { get; private set; }

        private float DistanceOffset = 1;

        public Vector3 WorldSize => new Vector3(GridWidth, GridHeight, GridLength) * PointDistance;

        public Vector3 Position { get; private set; }

        public Point[][][] Grid;

        Vector3 startPoint;

        public Pathfinding()
        {
        }

        private void AddNeighbour(Point p, Vector3Int neighbour)
        {
            if (neighbour.x > -1 && neighbour.x < GridWidth &&
               neighbour.y > -1 && neighbour.y < GridHeight &&
               neighbour.z > -1 && neighbour.z < GridLength)
            {
                p.Neighbours.Add(neighbour);
            }
        }

        public void UpdateGrid(Vector3 position, Vector3Int size, float pointDistance)
        {
            Position = position;
            GridWidth = size.x;
            GridHeight = size.y;
            GridLength = size.z;
            PointDistance = pointDistance;

            startPoint = new Vector3(-GridWidth, -GridHeight, -GridLength)
                / 2 * PointDistance + Vector3.one * PointDistance / 2 + Position;

            Debug.Log($"startPoint {startPoint}");
            Grid = new Point[GridWidth][][];
            for (int i = 0; i < GridWidth; i++)
            {
                Grid[i] = new Point[GridHeight][];
                for (int j = 0; j < GridHeight; j++)
                {
                    Grid[i][j] = new Point[GridLength];
                    for (int k = 0; k < GridLength; k++)
                    {
                        Vector3 pos = startPoint + new Vector3(i, j, k) * PointDistance;
                        Grid[i][j][k] = new Point();
                        Grid[i][j][k].Coords = new Vector3Int(i, j, k);
                        Grid[i][j][k].WorldPosition = pos;
                        if (Physics.CheckBox(Grid[i][j][k].WorldPosition, Vector3.one * (PointDistance) / 2f, Quaternion.identity))
                        {
                            Grid[i][j][k].Invalid = true;
                        }
                        for (int p = -1; p <= 1; p++)
                        {
                            for (int q = -1; q <= 1; q++)
                            {
                                for (int g = -1; g <= 1; g++)
                                {
                                    if (i == p && g == q && k == g)
                                    {
                                        continue;
                                    }
                                    AddNeighbour(Grid[i][j][k], new Vector3Int(i + p, j + q, k + g));
                                }
                            }
                        }
                    }
                }
            }
        }

        public Point[][][] GetGrid(List<Vector3Int> points, Vector3 position, Vector3Int size, float pointDistance)
        {
            Position = position;
            GridWidth = size.x;
            GridHeight = size.y;
            GridLength = size.z;
            PointDistance = pointDistance;

            startPoint = new Vector3(-GridWidth, -GridHeight, -GridLength)
                / 2 * PointDistance + Vector3.one * PointDistance / 2 + Position;

            //Debug.Log($"startPoint {startPoint}");
            var Grid = new Point[GridWidth][][];

            for (int i = 0; i < GridWidth; i++)
            {
                Grid[i] = new Point[GridHeight][];
                for (int j = 0; j < GridHeight; j++)
                {
                    Grid[i][j] = new Point[GridLength];
                    for (int k = 0; k < GridLength; k++)
                    {
                        Vector3 pos = startPoint + new Vector3(i, j, k) * PointDistance;
                        Grid[i][j][k] = new Point();
                        Grid[i][j][k].Coords = new Vector3Int(i, j, k);
                        Grid[i][j][k].WorldPosition = pos;

                        if (points.Contains(Grid[i][j][k].Coords))
                        {
                            Grid[i][j][k].Invalid = true;
                        }
                        for (int p = -1; p <= 1; p++)
                        {
                            for (int q = -1; q <= 1; q++)
                            {
                                for (int g = -1; g <= 1; g++)
                                {
                                    if (i == p && g == q && k == g)
                                    {
                                        continue;
                                    }
                                    AddNeighbour(Grid[i][j][k], new Vector3Int(i + p, j + q, k + g));
                                }
                            }
                        }
                    }
                }
            }

            return Grid;
        }

        public void DrawGizmos()
        {
            if (GridWidth > 0 && GridHeight > 0 && GridLength > 0 && PointDistance > 0)
            {
                Gizmos.DrawWireCube(Position, new Vector3(GridWidth - 1, GridHeight - 1, GridLength - 1) * PointDistance);

                if (Grid != null)
                {
                    for (int i = 0; i < GridWidth; i++)
                    {
                        for (int j = 0; j < GridHeight; j++)
                        {
                            for (int k = 0; k < GridLength; k++)
                            {
                                if (Grid[i][j][k].Invalid)
                                {
                                    Gizmos.color = Color.red;
                                    Gizmos.DrawSphere(Grid[i][j][k].WorldPosition, 0.1f * PointDistance);
                                }
                                else
                                {
                                    Gizmos.color = Color.green;
                                    Gizmos.DrawSphere(Grid[i][j][k].WorldPosition, 0.1f * PointDistance);
                                }
                            }
                        }
                    }
                }
            }
        }

        public Point GetClosestPointWorldSpace(Vector3 position)
        {
            if (Grid == null) return null;
            float sizeX = PointDistance * GridWidth;
            float sizeY = PointDistance * GridHeight;
            float sizeZ = PointDistance * GridLength;

            Vector3 pos = position - startPoint;
            float percentageX = Mathf.Clamp01(pos.x / sizeX);
            float percentageY = Mathf.Clamp01(pos.y / sizeY);
            float percentageZ = Mathf.Clamp01(pos.z / sizeZ);
            int x = Mathf.Clamp(Mathf.RoundToInt(percentageX * GridWidth), 0, GridWidth - 1);
            int y = Mathf.Clamp(Mathf.RoundToInt(percentageY * GridHeight), 0, GridHeight - 1);
            int z = Mathf.Clamp(Mathf.RoundToInt(percentageZ * GridLength), 0, GridLength - 1);
            Point result = Grid[x][y][z];
            while (result.Invalid)
            {
                int step = 1;
                List<Point> freePoints = new List<Point>();
                for (int p = -step; p <= step; p++)
                {
                    for (int q = -step; q <= step; q++)
                    {
                        for (int g = -step; g <= step; g++)
                        {
                            if (x == p && y == q && z == g)
                            {
                                continue;
                            }
                            int i = x + p;
                            int j = y + q;
                            int k = z + g;
                            if (i > -1 && i < GridWidth &&
                                j > -1 && j < GridHeight &&
                                k > -1 && k < GridLength)
                            {
                                if (!Grid[x + p][y + q][z + g].Invalid)
                                {
                                    freePoints.Add(Grid[x + p][y + q][z + g]);
                                }
                            }
                        }
                    }
                }
                float distance = Mathf.Infinity;
                for (int i = 0; i < freePoints.Count; i++)
                {
                    float dist = (freePoints[i].WorldPosition - position).sqrMagnitude;
                    if (dist < distance)
                    {
                        result = freePoints[i];
                        dist = distance;
                    }
                }
                if (freePoints.Count == 0)
                {
                    break;
                }
            }
            return result;
        }

        public List<Point> GetFreePoints()
        {
            List<Point> freePoints = new List<Point>();
            for (int i = 0; i < Grid.Length; i++)
            {
                for (int j = 0; j < Grid[i].Length; j++)
                {
                    for (int k = 0; k < Grid[i][j].Length; k++)
                    {
                        if (!Grid[i][j][k].Invalid)
                        {
                            freePoints.Add(Grid[i][j][k]);
                        }
                    }
                }
            }
            return freePoints;
        }
    }
}