using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.Navigation;
using Cysharp.Threading.Tasks;

public class PathController : MonoBehaviour
{
    [SerializeField] Vector3 startPosition;
    [SerializeField] CharacterMoveControl character;
    [SerializeField] int size = 10;
    [SerializeField] int distance = 10;

    private Vector3 _position;

    private void Start()
    {
        _position = startPosition;
        UpdatePath();
    }

    private async void UpdatePath()
    {
        Pathfinding.Instance.UpdateGrid(new Vector3(_position.x, _position.y + size * distance / 2, _position.z),
            Vector3Int.one * size, distance);
        var points = Pathfinding.Instance.GetFreePoints().FindAll(x=> x.Coords.z > Pathfinding.Instance.GridLength - 3);

        var randIndex = UnityEngine.Random.Range(0, points.Count);
        var target = points[randIndex].WorldPosition;
        //var status = await character.StartMove(target);

        //Debug.Log($"status {status}");
        _position = new Vector3(target.x, _position.y, target.z + (Pathfinding.Instance.WorldSize.z) / 2);
        UpdatePath();
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Pathfinding.Instance.DrawGizmos();
        }
    }
}
