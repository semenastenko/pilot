﻿using Cysharp.Threading.Tasks;
using Kudos.Player;
using System;
using UnityEngine;
using Zenject;
using UniRx;
using System.Collections.Generic;
using Kudos.Events;

namespace Kudos.Navigation
{
    [RequireComponent(typeof(Agent))]
    public class CharacterMoveControl : MonoBehaviour
    {
        Agent _Agent;

        private void Start()
        {
            _Agent = GetComponent<Agent>();
        }

        public async UniTask<AgentStatus> StartMove(Vector3 targetPosition)
        {
            _Agent.FindPath(targetPosition);

            await UniTask.WaitWhile(() => _Agent.Status == AgentStatus.InProgress);
            return _Agent.Status;
        }
    }
}