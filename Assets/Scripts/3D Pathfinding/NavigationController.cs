using Cysharp.Threading.Tasks;
using Kudos.Enemy;
using Kudos.Events;
using Kudos.SimpleGeneration;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = System.Random;

namespace Kudos.Navigation
{
    public class NavigationController : IDisposable, IGameEventHandler
    {
        private EnemyMoveControl _character;
        private WorldBuilder _worldBuilder;
        private int size;
        private int distance;
        private Vector2Int randomOffset = new Vector2Int(3, 3);

        private Vector3 startPosition;
        private Vector2Int chunkPosition;
        private Random Random;
        private Vector3Int lastTargetCoords;

        private bool isDispose;
        private bool waitPlayer;

        public event Action<IGameEvent> OnEvent;

        [Inject]
        public NavigationController(WorldBuilder worldBuilder, EnemyMoveControl character, ChunkPlacerSettings settings)
        {
            _character = character;
            _worldBuilder = worldBuilder;
            startPosition = character.transform.position;

            this.size = (int)settings.—hunkSpacing * 2 + 1;
            this.distance = (int)settings.—hunkSpacing / 2;

            SeedRandom.OnRandomizeSeed += () => { Random = SeedRandom.GetRandom(); };
            Random = SeedRandom.GetRandom();
        }

        private void Start()
        {
            waitPlayer = false;
        }

        public async void LoadWorld()
        {
            chunkPosition = Vector2Int.zero;
            _character.transform.position = startPosition;

            await _worldBuilder.ChunkPlacer.WaitLoadingChunks();
            var invalidPoints = await GetInvalidPointsAsync();
            var chunkCenterPosition = _worldBuilder.ChunkPlacer.Chunks[chunkPosition].transform.position;
            var grid = Pathfinding.Instance.GetGrid(invalidPoints, new Vector3(chunkCenterPosition.x, chunkCenterPosition.y + size * distance / 2, chunkCenterPosition.z),
               Vector3Int.one * size, distance);
            chunkPosition += Vector2Int.up;
            invalidPoints = await GetInvalidPointsAsync();

            OnEvent?.Invoke(new WorldStateEvent(WorldState.loaded));

            waitPlayer = true;
            await UniTask.WaitWhile(() => waitPlayer);
            isDispose = false;
            while (!isDispose)
            {
                var position = _worldBuilder.ChunkPlacer.Chunks[chunkPosition].transform.position;
                chunkPosition += Vector2Int.up;
                var result = await UpdatePath(invalidPoints, grid, position);
                invalidPoints = result.invalidPoints;
                grid = result.grid;
            }
        }

        private async UniTask<(List<Vector3Int> invalidPoints, Point[][][] grid)> UpdatePath(List<Vector3Int> invalidPoints, Point[][][] grid, Vector3 position)
        {
            Pathfinding.Instance.Grid = grid;
            var offset = randomOffset;
            var heightOffset = lastTargetCoords.y > (int)(Pathfinding.Instance.GridHeight * 0.75f) ?
                lastTargetCoords.y - (int)(Pathfinding.Instance.GridHeight * 0.75f) : 0;
            List<Point> points = null;
            while (points == null || points.Count == 0)
            {
                points = Pathfinding.Instance.GetFreePoints().FindAll(point =>
                point.Coords.z == Pathfinding.Instance.GridLength - 1 &&
                point.Coords.x > lastTargetCoords.x - offset.x && point.Coords.x < lastTargetCoords.x + offset.x &&
                point.Coords.y > lastTargetCoords.y - offset.y - heightOffset && point.Coords.y < lastTargetCoords.y + offset.y - heightOffset);
                offset += Vector2Int.one;
            }

            var randIndex = Random.Next(points.Count);
            var target = points[randIndex];
            lastTargetCoords = target.Coords;
            var result = await UniTask.WhenAll(
                _character.StartMove(target.WorldPosition),
                GetInvalidPointsAsync(),
                UniTask.Run(() => Pathfinding.Instance.GetGrid(invalidPoints, new Vector3(position.x, position.y + size * distance / 2, position.z),
                Vector3Int.one * size, distance)));

            Debug.Log($"UpdatePath Status {result.Item1}");
            return (result.Item2, result.Item3);
        }

        private async UniTask WaitForwardChunkLoad()
        {
            await UniTask.WaitUntil(() => _worldBuilder.ChunkPlacer.Chunks.ContainsKey(chunkPosition) && _worldBuilder.ChunkPlacer.Chunks.ContainsKey(chunkPosition + Vector2Int.up));
        }

        private async UniTask<List<Vector3Int>> GetInvalidPointsAsync()
        {
            await WaitForwardChunkLoad();
            return await UniTask.Run(() => GetInvalidPoints());
        }
        private List<Vector3Int> GetInvalidPoints()
        {
            List<Vector3Int> points = new List<Vector3Int>();
            foreach (var item in _worldBuilder.ChunkPlacer.Chunks[chunkPosition].Placed)
            {
                for (int i = 0; i <= size / (distance * 2); i++)
                {
                    for (int j = 0; j <= size / (distance * 2); j++)
                    {
                        for (int k = 0; k < item.height * (size / (distance * 2)) + 1; k++)
                        {
                            points.Add(new Vector3Int(item.pos.x * (size / (distance * 2)) + i, k, item.pos.y * (size / (distance * 2)) + j));
                        }
                    }
                }
            }
            return points;
        }

        private void OnDrawGizmos()
        {
            if (Application.isPlaying)
            {
                Pathfinding.Instance.DrawGizmos();
            }
        }

        public void Dispose()
        {
            isDispose = true;
            _character?.Dispose();
        }

        public void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is WorldStateEvent)
            {
                var state = (WorldStateEvent)gameEvent;
                if (state.State == WorldState.loading)
                {
                    LoadWorld();
                }
            }
            else if (gameEvent is GameStateEvent)
            {
                var state = (GameStateEvent)gameEvent;
                if (state.State == GameState.begin)
                {
                    Start();
                }
                else if (state.State == GameState.end)
                {
                    Dispose();
                }
                else if (state.State == GameState.reset)
                {
                    Dispose();
                    _worldBuilder.ChunkPlacer?.Dispose();
                }
            }

        }

        public int GetPriority() => 0;
    }
}