using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using Zenject;

namespace Kudos.Input
{
    public class ScreenControl : IDisposable
    {
        private Player.PlayerSettings _settings;
        private List<Touch> filteredFingers;
        private IDisposable dispose;

        public bool HasTouch { get; private set; }
        public float TapDirection { get; private set; }
        public Vector2 ScreenDelta { get; private set; }
        public Vector2 ScreenDeltaNormalized => new Vector2(2 * ScreenDelta.x / Screen.width, 2 * ScreenDelta.y / Screen.width) - new Vector2(UnityEngine.Input.GetAxis("Horizontal"), UnityEngine.Input.GetAxis("Vertical"));

        //public Vector2 InputZoneDelta => new Vector2(ScreenDelta.x, ScreenDelta.y) * _settings.InputSensitivity * 100 - new Vector2(UnityEngine.Input.GetAxis("Horizontal"), UnityEngine.Input.GetAxis("Vertical"));
        public Vector2 InputZoneDelta => new Vector2(
            Mathf.Clamp(ScreenDeltaNormalized.x / _settings.ZoneRange, -1, 1),
            Mathf.Clamp(ScreenDeltaNormalized.y / _settings.ZoneRange, -1, 1));
        public Vector2 PermanentZoneDelta { get; private set; }

        [Inject]
        public ScreenControl(Player.PlayerSettings settings)
        {
            _settings = settings;
            dispose = Observable.EveryUpdate().Subscribe(_ => Update());
        }

        private void Update()
        {
            var fingers = SimulatedInput.UpdateAndGetFingers();
            ScreenDelta = SimulatedInput.GetScreenDelta(fingers, true);
            TapDirection = 0;
            HasTouch = fingers.Count != 0;
            foreach (var finger in fingers)
            {
                switch (finger.phase)
                {
                    case TouchPhase.Stationary:
                    case TouchPhase.Moved:
                    case TouchPhase.Ended:
                        PermanentZoneDelta = InputZoneDelta;
                        //if (finger.position.x < Screen.width / 3)
                        //    TapDirection = -1;
                        //else if (finger.position.x > 2 * Screen.width / 3)
                        //    TapDirection = 1;
                        break;
                    default:
                        break;
                }
            }
        }

        public void Reset()
        {
            PermanentZoneDelta = Vector2.zero;
        }

        public void Dispose()
        {
            dispose?.Dispose();
        }

    }
}
