using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Kudos.Input
{
    public static class SimulatedInput
    {
        static bool touchSupported => UnityEngine.Input.touchSupported;

        static SimulateTouchWithMouse simulator = new SimulateTouchWithMouse();

        static Touch? fakeTouch => simulator.FakeTouch;

        private static List<Touch> filteredFingers;

        public static bool GetButton(string buttonName)
        {
            return UnityEngine.Input.GetButton(buttonName);
        }

        public static bool GetButtonDown(string buttonName)
        {
            return UnityEngine.Input.GetButtonDown(buttonName);
        }

        public static bool GetButtonUp(string buttonName)
        {
            return UnityEngine.Input.GetButtonUp(buttonName);
        }

        public static bool GetMouseButton(int button)
        {
            return UnityEngine.Input.GetMouseButton(button);
        }

        public static bool GetMouseButtonDown(int button)
        {
            return UnityEngine.Input.GetMouseButtonDown(button);
        }

        public static bool GetMouseButtonUp(int button)
        {
            return UnityEngine.Input.GetMouseButtonUp(button);
        }

        public static int touchCount
        {
            get
            {
                if (touchSupported)
                {
                    return UnityEngine.Input.touchCount;
                }
                else
                {
                    return fakeTouch.HasValue ? 1 : 0;
                }
            }
        }

        public static Touch GetTouch(int index)
        {
            if (touchSupported)
            {
                return UnityEngine.Input.GetTouch(index);
            }
            else
            {
                Assert.IsTrue(fakeTouch.HasValue && index == 0);
                return fakeTouch.Value;
            }
        }

        public static Touch[] touches
        {
            get
            {
                if (touchSupported)
                {
                    return UnityEngine.Input.touches;
                }
                else
                {
                    return fakeTouch.HasValue ? new[] { fakeTouch.Value } : new Touch[0];
                }
            }
        }

        public static List<Touch> UpdateAndGetFingers()
        {
            if (filteredFingers == null)
            {
                filteredFingers = new List<Touch>();
            }

            filteredFingers.Clear();

            filteredFingers.AddRange(GetFingers());

            return filteredFingers;
        }

        private static List<Touch> GetFingers()
        {
            List<Touch> Fingers = new List<Touch>();

            for (int i = 0; i < UnityEngine.Input.touchCount; i++)
            {
                var touch = UnityEngine.Input.GetTouch(i);

                Fingers.Add(touch);
            }

#if UNITY_EDITOR
            for (int i = 0; i < SimulatedInput.touchCount; i++)
            {
                var touch = SimulatedInput.GetTouch(i);

                Fingers.Add(touch);
            }
#endif

            return Fingers;
        }

        public static Vector2 GetScreenDelta(List<Touch> fingers, bool isRaw = false)
        {
            var delta = default(Vector2);
            TryGetScreenDelta(fingers, ref delta, isRaw);
            return delta;
        }

        private static bool TryGetScreenDelta(List<Touch> fingers, ref Vector2 delta, bool isRaw)
        {
            if (fingers != null)
            {
                var total = Vector2.zero;
                var count = 0;

                for (var i = fingers.Count - 1; i >= 0; i--)
                {
                    var finger = fingers[i];
                    if (isRaw)
                        total += finger.rawPosition - finger.position;
                    else
                        total += finger.deltaPosition;
                    count += 1;
                }

                if (count > 0)
                {
                    delta = total / count; return true;
                }
            }

            return false;
        }
    }

    public class SimulateTouchWithMouse
    {
        private float lastUpdateTime;
        private Vector3 prevMousePos;
        private Vector3 rawMousePos;
        private Touch? fakeTouch;

        public Touch? FakeTouch
        {
            get
            {
                Update();
                return fakeTouch;
            }
        }

        void Update()
        {
            if (Time.time != lastUpdateTime)
            {
                lastUpdateTime = Time.time;

                var curMousePos = UnityEngine.Input.mousePosition;
                var delta = curMousePos - prevMousePos;
                prevMousePos = curMousePos;
                if (UnityEngine.Input.GetMouseButtonDown(0))
                {
                    rawMousePos = UnityEngine.Input.mousePosition;
                }
                fakeTouch = CreateTouch(GetPhase(delta), delta, rawMousePos);
            }
        }

        static TouchPhase? GetPhase(Vector3 delta)
        {
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                return TouchPhase.Began;
            }
            else if (UnityEngine.Input.GetMouseButton(0))
            {
                return delta.sqrMagnitude < 0.01f ? TouchPhase.Stationary : TouchPhase.Moved;
            }
            else if (UnityEngine.Input.GetMouseButtonUp(0))
            {
                return TouchPhase.Ended;
            }
            else
            {
                return null;
            }
        }

        static Touch? CreateTouch(TouchPhase? phase, Vector3 delta, Vector3 rawPos)
        {
            if (!phase.HasValue)
            {
                return null;
            }

            var curMousePos = UnityEngine.Input.mousePosition;
            return new Touch
            {
                phase = phase.Value,
                type = TouchType.Indirect,
                position = curMousePos,
                rawPosition = rawPos,
                fingerId = 0,
                tapCount = 1,
                deltaTime = Time.deltaTime,
                deltaPosition = delta
            };
        }

    }
}

