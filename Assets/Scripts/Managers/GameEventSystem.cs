using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Kudos.Player;
using Kudos.Events;
using Kudos.Navigation;
using Cysharp.Threading.Tasks;
using UniRx;
using System;

namespace Kudos.Managers
{
    public class GameEventSystem
    {
        private IGameEventReceiver[] _receivers;

        [Inject]
        public GameEventSystem(IGameEventReceiver[] receivers, IGameEventSender[] senders)
        {
            Array.Sort(receivers, (x, y) => x.GetPriority().CompareTo(y.GetPriority()));
            _receivers = receivers;
            foreach (var sender in senders)
            {
                sender.OnEvent += SendEvent;
            }
        }

        public void SendEvent(IGameEvent gameEvent)
        {
            foreach (var item in _receivers)
            {
                item.ReceiveEvent(gameEvent);
            }
        }
    }
}
