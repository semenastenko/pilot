using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;
using Cysharp.Threading.Tasks;

namespace Kudos.Events
{
    public interface IGameEvent
    {
        object GetArgument();
    }

    public interface IGameEventSender
    {
        event Action<IGameEvent> OnEvent;
    }

    public interface IGameEventReceiver
    {
        void ReceiveEvent(IGameEvent gameEvent);
        int GetPriority();
    }

    public interface IGameEventHandler : IGameEventReceiver, IGameEventSender
    {
    }
}
