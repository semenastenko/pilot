using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.Managers;

namespace Kudos.Events
{
    public enum GameState { begin, end, reset }
    public struct GameStateEvent : IGameEvent
    {
        public readonly GameState State;

        public GameStateEvent(GameState state)
        {
            State = state;
        }

        public object GetArgument() => State;
    }


    public enum WinState { lose, winStart, winEnd }
    public struct EndGameStateEvent : IGameEvent
    {
        public readonly WinState State;

        public EndGameStateEvent(WinState state)
        {
            State = state;
        }

        public object GetArgument() => State;
    }

    public struct PauseStateEvent : IGameEvent
    {
        public readonly bool IsPause;

        public PauseStateEvent(bool isPause)
        {
            IsPause = isPause;
        }

        public object GetArgument() => IsPause;
    }

    public enum WorldState { loading, loaded }

    public struct WorldStateEvent : IGameEvent
    {
        public readonly WorldState State;

        public WorldStateEvent(WorldState state)
        {
            State = state;
        }
        public object GetArgument() => State;
    }
}
