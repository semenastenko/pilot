using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextColorizer
{
    public static string SetColor(string text, Color color)
        => $"<color=#{ColorUtility.ToHtmlStringRGB(color)}>{text}</color>";

}
