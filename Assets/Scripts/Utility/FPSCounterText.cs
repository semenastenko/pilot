using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounterText : Text
{
    void Update()
    {
        if (!Application.isPlaying) text = "666";
        else text = ((int)(1f / Time.unscaledDeltaTime)).ToString();
    }
}
