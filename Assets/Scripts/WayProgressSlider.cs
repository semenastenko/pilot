using Kudos.Enemy;
using Kudos.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class WayProgressSlider : MonoBehaviour
{
    [Inject] EnemyMoveControl _enemy;
    private Slider outzoneSlider;
    private Slider waySlider;
    private Text outzoneText;

    private void Awake()
    {
        outzoneSlider = GetComponent<Slider>();
        waySlider = transform.GetChild(0).GetComponent<Slider>();
        outzoneText = GetComponentInChildren<Text>();
    }

    private void Update()
    {
        var result = _enemy.GetDistanceInPercent();
        outzoneSlider.value = result.value;
        waySlider.value = _enemy.DistanceProgress();
        if (result.outzoneTimer == 0)
        {
            outzoneText.text = "";
        }
        else
        {
            outzoneText.text = string.Format("{0:0.0}", result.outzoneMaxTimer - result.outzoneTimer);
        }
    }
}
