﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Kudos.SimpleGeneration
{
    public struct ChunkElementPlacer
    {
        public static ChunkElementSettings Setting { get; set; }
        private List<(MeshFilter parent, List<MeshFilter> children)> meshes;

        public ChunkElementPlacer(Transform parent, int variantCount)
        {
            meshes = new();
            for (int i = 0; i < variantCount; i++)
            {
                var filter = new GameObject($"Combine Buildings {i}").AddComponent<MeshFilter>();
                filter.gameObject.AddComponent<MeshRenderer>();
                filter.transform.SetParent(parent);
                filter.transform.localPosition = Vector3.zero;
                meshes.Add((filter, new List<MeshFilter>()));
            }
        }

        public ChunkElement Place(int index, Vector3 position, Vector2 size)
        {
            if (Setting.Prefabs == null || Setting.Prefabs.Length == 0 || index >= Setting.Prefabs.Length) return null;
            var spawnedChunk = GameObject.Instantiate(Setting.Prefabs[index], position + new Vector3(size.x, size.y, size.x) / 2, Quaternion.identity);
            spawnedChunk.transform.localScale = new Vector3(size.x, size.y, size.x);
            spawnedChunk.transform.SetParent(meshes[index].parent.transform);
            meshes[index].children.Add(spawnedChunk.GetComponent<MeshFilter>());
            return spawnedChunk;
        }

        public void CombineMeshes()
        {
            for (int i = 0; i < meshes.Count; i++)
            {
                CombineInstance[] combine = new CombineInstance[meshes[i].children.Count];

                for (int j = 0; j < meshes[i].children.Count; j++)
                {
                    combine[j].mesh = meshes[i].children[j].sharedMesh;
                    combine[j].transform = meshes[i].parent.transform.worldToLocalMatrix * meshes[i].children[j].transform.localToWorldMatrix;
                    meshes[i].children[j].gameObject.SetActive(false);
                }

                meshes[i].parent.mesh = new Mesh();
                meshes[i].parent.mesh.CombineMeshes(combine);
                meshes[i].parent.GetComponent<MeshRenderer>().material = Setting.Prefabs[i].GetComponent<Renderer>().sharedMaterial;
                meshes[i].parent.gameObject.layer = LayerMask.NameToLayer("Radar");
                meshes[i].parent.gameObject.AddComponent<MeshCollider>();
                meshes[i].parent.gameObject.SetActive(true);
            }
        }
    }
}