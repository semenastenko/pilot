using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public static class SeedRandom
{
    public static string seed;

    public static event Action OnRandomizeSeed;

    public static void RandomizeSeed()
    {
        seed = DateTime.Now.GetHashCode().ToString();
        OnRandomizeSeed?.Invoke();
    }

    public static Random GetRandom() => new Random(seed.GetHashCode());
    public static Random GetRandom(string customSeed) => new Random(customSeed.GetHashCode());

    public static double Next(Random random, double minimum, double maximum) =>
        random.NextDouble() * (maximum - minimum) + minimum;

    public static int Next(Random random, int minimum, int maximum) =>
        random.Next(minimum, maximum);

}

public class WeightedRandom<T>
{
    private struct Entry
    {
        public double accumulatedWeight;
        public T item;
    }

    private List<Entry> entries = new List<Entry>();
    private double accumulatedWeight;
    private Random _random;

    public WeightedRandom()
    {
        _random = new Random();
    }

    public WeightedRandom(Random random)
    {
        _random = random;
    }

    public void AddEntry(T item, double weight)
    {
        accumulatedWeight += weight;
        entries.Add(new Entry { item = item, accumulatedWeight = accumulatedWeight });
    }

    public T GetRandom()
    {
        double r = _random.NextDouble() * accumulatedWeight;

        foreach (Entry entry in entries)
        {
            if (entry.accumulatedWeight >= r)
            {
                return entry.item;
            }
        }
        return default(T);
    }

    public void Clear()
    {
        entries.Clear();
        accumulatedWeight = 0;
    }

    public IEnumerator<T> GetEnumerator() => entries.Select(x=>x.item).GetEnumerator();
}