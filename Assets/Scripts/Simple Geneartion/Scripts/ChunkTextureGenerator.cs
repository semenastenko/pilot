﻿using UnityEngine;
using System.Linq;

namespace Kudos.SimpleGeneration
{
    public class ChunkTextureGenerator
    {
        public static string roadColorHEX = "#555555";
        public static string placementColorHEX = "#009105";

        public static Color roadColor;
        public static Color placementColor;

        private static void UpdateColor()
        {
            ColorUtility.TryParseHtmlString(roadColorHEX, out roadColor);
            ColorUtility.TryParseHtmlString(placementColorHEX, out placementColor);
        }

        public static Texture2D Generate(Vector2Int[] positions, int gridSize, float margin, int detalization)
        {
            UpdateColor();
            //Texture2D texture = new Texture2D(textureSize, textureSize);
            Texture2D texture = new Texture2D(gridSize * detalization, gridSize * detalization);
            texture.filterMode = FilterMode.Point;
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.SetPixels(UpdateColorMap(Fill(gridSize, detalization), positions, gridSize, margin, detalization));
            texture.Apply();
            return texture;
        }

        public static void Update(ref Texture2D texture, Vector2Int[] positions, int gridSize, float margin, int detalization)
        {
            UpdateColor();
            Color[,] colors = new Color[texture.width, texture.height];
            for (int x = 0; x < texture.width; x++)
            {
                for (int y = 0; y < texture.height; y++)
                {
                    colors[x, y] = texture.GetPixel(x, y);
                }
            }
            texture.SetPixels(UpdateColorMap(colors, positions, gridSize, margin, detalization));
            texture.Apply();
        }

        private static Color[,] Fill(int gridSize, int detalization)
        {
            Color[,] colors = new Color[gridSize * detalization, gridSize * detalization];
            for (int i = 0; i < gridSize * detalization; i++)
                for (int j = 0; j < gridSize * detalization; j++)
                    colors[i, j] = roadColor;

            return colors;
        }

        private static Color[] UpdateColorMap(Color[,] colors, Vector2Int[] positions, int gridSize, float margin, int detalization)
        {
            var deltaMargine = (int)(margin * detalization);

            foreach (var position in positions)
            {
                for (int x = -deltaMargine; x < detalization + deltaMargine; x++)
                {
                    for (int y = -deltaMargine; y < detalization + deltaMargine; y++)
                    {
                        if (position.x == 10)
                        {

                        }
                        var deltaX = (gridSize - position.y) * detalization - y - 1;
                        var deltaY = (gridSize - position.x) * detalization - x - 1;
                        if (deltaX >= 0 && deltaX < gridSize * detalization && deltaY >= 0 && deltaY < gridSize * detalization)
                        {
                            colors[deltaX, deltaY] = placementColor;
                        }
                    }
                }
            }

            return colors.Cast<Color>().ToArray();
        }
    }
}