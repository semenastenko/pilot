﻿using System;
using UnityEngine;

namespace Kudos.SimpleGeneration
{
    [Serializable]
    public class ChunkElementSettings
    {
        [SerializeField] ChunkElement[] prefabs;
        [SerializeField] int minHeight = 1;
        [SerializeField] int maxHeight = 10;
        public ChunkElement[] Prefabs => prefabs;
        public int MinHeight => minHeight;
        public int MaxHeight => maxHeight;
    }

    [Serializable]
    public class ChunkSettings
    {
        [SerializeField] Chunk chunkPrefab;
        [SerializeField] float size = 10;
        [SerializeField] int gridSize = 10;
        [SerializeField] int detalizationLevel = 3;
        [SerializeField, Range(0, 1)] float marginPercent = 1f/3f;
        [SerializeField] ChunkElementSettings elementSettings;

        public Chunk ChunkPrefab => chunkPrefab;
        public float Size => size;
        public int GridSize => gridSize;
        public int DetalizationLevel => detalizationLevel;
        public float MarginPercent => marginPercent;
        public ChunkElementSettings ElementSettings => elementSettings;
    }


    [CreateAssetMenu(fileName = "ChunkPlacerSettings", menuName = "SO/ChunkPlacerSettings")]
    public class ChunkPlacerSettings : ScriptableObject
    {
        [SerializeField] Vector2 visibleDistance;
        [SerializeField] Vector3 distanceOffset;
        [SerializeField] float chunkSpacing = 10;
        [SerializeField] ChunkSettings chunk;

        public Vector2 VisibleDistance => visibleDistance * GridSize;
        public Vector3 DistanceOffset => distanceOffset * GridSize;
        public float СhunkSpacing => chunkSpacing;
        public float GridSize => chunk.Size * СhunkSpacing;
        public ChunkSettings Chunk => chunk;
    }
}
