﻿using Kudos.Player;
using UnityEngine;
using Zenject;

namespace Kudos.SimpleGeneration
{
    public class WorldBuilder
    {
        public readonly ChunkPlacer ChunkPlacer;
        public WorldBuilder(PlayerPlane target, ChunkPlacerSettings settings, RandomSettings randomSettings)
        {
            ChunkPlacer = new ChunkPlacer(target.transform, settings, randomSettings);
            target.ChunkPlacer = ChunkPlacer;
        }
    }
}
