using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Random = System.Random;
using UnityEngine.AI;

namespace Kudos.SimpleGeneration
{
    public class Chunk : MonoBehaviour
    {
        public static class Factory
        {
            public async static UniTask<Chunk> Create(Vector2Int position, ((Vector2Int pos, int height) nearby, Vector2Int direction)[] nearby)
            {
                Vector3 deltaPos = Settings.GridSize * Settings.Size * new Vector3(position.x, 0, position.y);
                var spawnedChunk = GameObject.Instantiate(Settings.ChunkPrefab, deltaPos, Quaternion.identity);
                spawnedChunk.transform.localScale = Vector3.one * Settings.Size;
                spawnedChunk.name = $"Chunk {position}";
                spawnedChunk.Position = position;
                spawnedChunk._random = SeedRandom.GetRandom(SeedRandom.seed + position);
                spawnedChunk.ResetVacancy();
                spawnedChunk.AddNearbyPlaces(nearby);
                if (position != Vector2Int.zero)
                {
                    await FillChunk(spawnedChunk);
                }
                spawnedChunk.SetTexture();

                return spawnedChunk;
            }
            private static int jobCount = 0;
            private async static UniTask FillChunk(Chunk chunk)
            {
                if (ChunkElementPlacer.Setting == null)
                {
                    ChunkElementPlacer.Setting = Settings.ElementSettings;
                }
                var placer = new ChunkElementPlacer(chunk.transform, Settings.ElementSettings.Prefabs.Length);

                int whileCount = 0;

                while (chunk.vacancy.Count > 0 && whileCount < Settings.GridSize * Settings.GridSize)
                {
                    var places = chunk.GetVacancyPlace(chunk.vacancy[0]);
                    var randIndex = chunk._random.Next(0, Settings.ElementSettings.Prefabs.Length);
                    var height = chunk._random.Next(Settings.ElementSettings.MinHeight, Settings.ElementSettings.MaxHeight);

                    foreach (var place in places)
                    {
                        var position = chunk.transform.position - new Vector3(Settings.Size * Settings.GridSize, 0, Settings.Size * Settings.GridSize) / 2
                            + new Vector3(place.x, chunk.transform.position.y, place.y) * Settings.Size;
                        placer.Place(randIndex, position, new Vector2(Settings.Size, height * Settings.Size));
                        chunk.placed.Add((place, height));
                        if (IsTargetFPS())
                            await UniTask.Yield();
                    }
                    whileCount++;
                }

                placer.CombineMeshes();
            }

            public async static UniTask Destroy(Chunk chunk)
            {
                GameObject.Destroy(chunk.gameObject);
                if (IsTargetFPS())
                    await UniTask.Yield();
            }

            private static bool IsTargetFPS()
            {
                //return Time.unscaledDeltaTime > 1f / Application.targetFrameRate;
                if (jobCount < 10)
                {
                    jobCount++;
                    return false;
                }
                else
                {
                    jobCount = 0;
                    return true;
                }
            }
        }

        public static ChunkSettings Settings { get; set; }
        public static RandomSettings RandomSettings { get; set; }
        public Vector2Int Position { get; private set; }

        private List<Vector2Int> vacancy = new();
        private List<(Vector2Int pos, int height)> placed = new();

        private Random _random;

        public List<Vector2Int> Vacancy => vacancy;
        public List<(Vector2Int pos, int height)> Placed => placed;

        private void AddNearbyPlaces(((Vector2Int pos, int height) nearby, Vector2Int direction)[] nearby)
        {
            foreach (var near in nearby)
            {
                placed.Add((near.nearby.pos, near.nearby.height));
                vacancy.Remove(near.nearby.pos - near.direction);
            }
        }

        private void SetTexture()
        {
            GetComponent<Renderer>().material.mainTexture = ChunkTextureGenerator.Generate(placed.Select(x=>x.pos).ToArray(), Settings.GridSize, Settings.MarginPercent, Settings.DetalizationLevel);
        }

        public void UpdateTexture(((Vector2Int pos, int height) nearby, Vector2Int direction)[] nearby)
        {
            if (nearby.Length > 0)
            {
                AddNearbyPlaces(nearby);
                SetTexture();
            }
        }

        private void ResetVacancy()
        {
            for (int x = 0; x < Settings.GridSize; x++)
            {
                for (int z = 0; z < Settings.GridSize; z++)
                {
                    vacancy.Add(new Vector2Int(x, z));
                }
            }
        }

        private Vector2Int[] GetVacancyPlace(Vector2Int pos)
        {
            List<Vector2Int> places = new();

            var direction = GetDirection();
            var inverseDir = new Vector2(direction.y, direction.x);
            var width = SeedRandom.Next(_random, RandomSettings.Width.x, RandomSettings.Width.y);
            var length = SeedRandom.Next(_random, RandomSettings.Length.x, RandomSettings.Length.y);
            var roadSize = SeedRandom.Next(_random, RandomSettings.RoadSize.x, RandomSettings.RoadSize.y);
            for (int i = -roadSize; i < width + roadSize; i++)
            {
                for (int j = -roadSize; j < length + roadSize; j++)
                {
                    var index = vacancy.FindIndex(x => x == pos + direction * i + inverseDir * j);
                    if (index >= 0)
                    {
                        if (i >= 0 && i < width && j >= 0 && j < length)
                        {
                            places.Add(vacancy[index]);
                        }
                        vacancy.RemoveAt(index);
                    }
                }
            }

            return places.ToArray();
        }

        private Vector2Int GetDirection()
        {
            var random = new WeightedRandom<Vector2Int>(_random);
            random.AddEntry(Vector2Int.right, RandomSettings.DirectionWeight);
            random.AddEntry(Vector2Int.up, 100 - RandomSettings.DirectionWeight);

            return random.GetRandom();
        }

        public ((Vector2Int pos, int height) nearby, Vector2Int direction)[] GetChunkBorder(Vector2Int dir)
        {
            List<((Vector2Int pos, int height) nearby, Vector2Int direction)> chunkBorder = new List<((Vector2Int pos, int height) nearby, Vector2Int direction)>();

            for (int i = 0; i < Settings.GridSize; i++)
            {
                var index = -1;
                if (dir == Vector2Int.up)
                {
                    index = placed.FindIndex(x => x.pos == new Vector2Int(i, 0));
                }
                else if (dir == Vector2Int.down)
                {
                    index = placed.FindIndex(x => x.pos == new Vector2Int(i, Settings.GridSize - 1));
                }
                else if (dir == Vector2Int.right)
                {
                    index = placed.FindIndex(x => x.pos == new Vector2Int(0, i));
                }
                else if (dir == Vector2Int.left)
                {
                    index = placed.FindIndex(x => x.pos == new Vector2Int(Settings.GridSize - 1, i));
                }
                if (index >= 0)
                {
                    chunkBorder.Add(((placed[index].pos + dir * Settings.GridSize, placed[index].height), dir));
                }
            }

            return chunkBorder.ToArray();
        }
    }
}