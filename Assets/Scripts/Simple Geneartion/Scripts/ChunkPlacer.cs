using Cysharp.Threading.Tasks;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Kudos.SimpleGeneration
{

    public class ChunkPlacer : IDisposable
    {
        private readonly Dictionary<Vector2Int, Chunk> Map = new();
        private readonly Queue<Vector2Int> RemoveQueue = new();
        private readonly Transform Target;
        private readonly ChunkPlacerSettings Settings;
        private readonly RandomSettings RandomSettings;

        private bool isLoadingChunks = false;

        public Random Random { get; set; }

        public Dictionary<Vector2Int, Chunk> Chunks => Map;

        public ChunkPlacer(Transform target, ChunkPlacerSettings settings, RandomSettings randomSettings)
        {
            SeedRandom.OnRandomizeSeed += () => { Random = SeedRandom.GetRandom(); };
            Random = SeedRandom.GetRandom();
            Target = target;
            Settings = settings;
            RandomSettings = randomSettings;
            Chunk.Settings = settings.Chunk;
            Chunk.RandomSettings = randomSettings;
            Start();
        }

        public async UniTask WaitLoadingChunks()
        {
            await UniTask.WaitWhile(() => isLoadingChunks);
        }

        private async void Start()
        {
            while (Application.isPlaying)
            {
                var targetPosition = Target.transform.position + Settings.DistanceOffset;
                await GCElements(targetPosition);
                await VisibleDistanceMapping(targetPosition);
                await UniTask.Yield();
            }
        }

        private async UniTask GCElements(Vector3 targetPosition)
        {
            foreach (var item in Map)
            {
                if (item.Key.y < (int)((targetPosition.z - Settings.VisibleDistance.y) / Settings.GridSize)
                    || item.Key.y > (int)((targetPosition.z + Settings.VisibleDistance.y) / Settings.GridSize)
                    || item.Key.x < (int)((targetPosition.x - Settings.VisibleDistance.x) / Settings.GridSize)
                    || item.Key.x > (int)((targetPosition.x + Settings.VisibleDistance.x) / Settings.GridSize))
                {
                    RemoveQueue.Enqueue(item.Key);
                }
            }

            while (RemoveQueue.Count != 0)
            {
                var removing = RemoveQueue.Dequeue();
                await RemoveChunk(removing);
            }
        }

        private async UniTask VisibleDistanceMapping(Vector3 targetPosition)
        {
            int iterationCount = 0;
            int containsCount = 0;
            for (int z = (int)((targetPosition.z - Settings.VisibleDistance.y) / Settings.GridSize); z <= (int)((targetPosition.z + Settings.VisibleDistance.y) / Settings.GridSize); z++)
            {
                for (int x = (int)((targetPosition.x - Settings.VisibleDistance.x) / Settings.GridSize); x <= (int)((targetPosition.x + Settings.VisibleDistance.x) / Settings.GridSize); x++)
                {
                    iterationCount++;
                    var pos = new Vector2Int(x, z);
                    if (!Map.ContainsKey(pos))
                    {
                        containsCount++;
                        isLoadingChunks = true;
                        await PlaceChunk(pos);
                    }
                }
            }
            if (iterationCount == containsCount)
                isLoadingChunks = false;
        }

        private async UniTask RemoveChunk(Vector2Int pos)
        {
            if (Map.ContainsKey(pos))
            {
                var chunk = Map[pos];
                Map.Remove(pos);
                await Chunk.Factory.Destroy(chunk);
            }
        }

        private async UniTask PlaceChunk(Vector2Int position)
        {
            var nearbyPlaces = GetNearbyPlaces(position);
            var spawnedChunk = await Chunk.Factory.Create(position, nearbyPlaces);
            Map.Add(position, spawnedChunk);

            UpdateNearbyChunks(spawnedChunk);
        }

        private ((Vector2Int pos, int height) nearby, Vector2Int direction)[] GetNearbyPlaces(Vector2Int position)
        {
            List<((Vector2Int pos, int height) nearby, Vector2Int direction)> nearbyPlaces = new List<((Vector2Int pos, int height) nearby, Vector2Int direction)>();
            if (Map.TryGetValue(position + Vector2Int.up, out Chunk upChunk))
            {
                nearbyPlaces.AddRange(upChunk.GetChunkBorder(Vector2Int.up));
            }
            if (Map.TryGetValue(position + Vector2Int.down, out Chunk downChunk))
            {
                nearbyPlaces.AddRange(downChunk.GetChunkBorder(Vector2Int.down));
            }
            if (Map.TryGetValue(position + Vector2Int.right, out Chunk rightChunk))
            {
                nearbyPlaces.AddRange(rightChunk.GetChunkBorder(Vector2Int.right));
            }
            if (Map.TryGetValue(position + Vector2Int.left, out Chunk leftChunk))
            {
                nearbyPlaces.AddRange(leftChunk.GetChunkBorder(Vector2Int.left));
            }
            return nearbyPlaces.ToArray();
        }

        private void UpdateNearbyChunks(Chunk chunk)
        {
            if (Map.TryGetValue(chunk.Position + Vector2Int.up, out Chunk upChunk))
            {
                upChunk.UpdateTexture(chunk.GetChunkBorder(Vector2Int.down));
            }
            if (Map.TryGetValue(chunk.Position + Vector2Int.down, out Chunk downChunk))
            {
                downChunk.UpdateTexture(chunk.GetChunkBorder(Vector2Int.up));
            }
            if (Map.TryGetValue(chunk.Position + Vector2Int.right, out Chunk rightChunk))
            {
                rightChunk.UpdateTexture(chunk.GetChunkBorder(Vector2Int.left));
            }
            if (Map.TryGetValue(chunk.Position + Vector2Int.left, out Chunk leftChunk))
            {
                leftChunk.UpdateTexture(chunk.GetChunkBorder(Vector2Int.right));
            }
        }

        public void Dispose()
        {
            foreach (var item in Map)
            {
                RemoveQueue.Enqueue(item.Key);
            }

            while (RemoveQueue.Count != 0)
            {
                var removing = RemoveQueue.Dequeue();
                RemoveChunk(removing);
            }
        }
    }
}