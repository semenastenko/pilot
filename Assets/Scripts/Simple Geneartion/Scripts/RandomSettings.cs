﻿using UnityEngine;

namespace Kudos.SimpleGeneration
{
    [CreateAssetMenu(fileName = "RandomSettings", menuName = "SO/RandomSettings")]
    public class RandomSettings : ScriptableObject
    {
        [SerializeField, Range(0, 100)] float directionWeight;
        [SerializeField, MinMaxSlider(0, 10)] Vector2Int roadSize;
        [SerializeField, MinMaxSlider(0, 10)] Vector2Int length;
        [SerializeField, MinMaxSlider(0, 10)] Vector2Int width;

        public float DirectionWeight => directionWeight;
        public Vector2Int RoadSize => roadSize;
        public Vector2Int Length => length;
        public Vector2Int Width => width;
    }
}
