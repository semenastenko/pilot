using DG.Tweening;
using MVVMUI.Elements;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Kudos.MVVMUI
{
    public class SwitchToggleModify : MonoBehaviour
    {
        [SerializeField] SwitchToggle toggle;
        [SerializeField] Image handlerImage;
        [SerializeField] Sprite onSprite;
        [SerializeField] Sprite offSprite;

        private void OnEnable()
        {
            UpdateValue();
            toggle.PropertyChanged += Toggle_PropertyChanged;
        }

        private async void Toggle_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateValue();
            if (toggle.IsOn)
            {
                toggle.value = 0;
                _ = DOTween.To(() => toggle.value, x => toggle.value = x, 1, .5f)
                           .SetEase(Ease.OutBack)
                           .AsyncWaitForCompletion();
            }
            else
            {
                toggle.value = 1;
                _ = DOTween.To(() => toggle.value, x => toggle.value = x, 0, .5f)
                           .SetEase(Ease.OutBack)
                           .AsyncWaitForCompletion();
            }
            await handlerImage.transform.DOScaleX(0.9f, .25f).SetEase(Ease.InBack).AsyncWaitForCompletion();
            _ = handlerImage.transform.DOScaleX(1, .25f).SetEase(Ease.OutBack).AsyncWaitForCompletion();
        }

        private void UpdateValue()
        {
            if (toggle.IsOn)
            {
                handlerImage.sprite = onSprite;
            }
            else
            {
                handlerImage.sprite = offSprite;
            }
        }

        private void OnDisable()
        {
            toggle.PropertyChanged -= Toggle_PropertyChanged;
        }
    }
}
