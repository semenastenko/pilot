using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class MainMenuPanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(MainMemuViewModel);
    }
}