﻿using Kudos.Events;
using MVVMUI;
using System;
using UnityEngine;

namespace Kudos.MVVMUI
{
    public class StartLoadingPanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(WorldLoadingPanelView);       
    }
}