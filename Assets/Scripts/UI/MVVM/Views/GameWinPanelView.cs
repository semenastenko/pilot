﻿using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class GameWinPanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(GameWinViewModel);
    }
}