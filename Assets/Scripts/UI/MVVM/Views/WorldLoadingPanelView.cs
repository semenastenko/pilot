﻿using Kudos.Events;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using UnityEngine;

namespace Kudos.MVVMUI
{
    [Injection]
    public class WorldLoadingPanelView : UIPanel, IGameEventHandler
    {
        public Type loadingPanel;
        public Type LoadingPanel
        {
            get => loadingPanel;
            set
            {
                if (loadingPanel == value)
                    return;
                loadingPanel = value;
                if (loadingPanel != null)
                    OnEvent?.Invoke(new WorldStateEvent(WorldState.loading));
            }
        }

        public event Action<IGameEvent> OnEvent;

        public override Type GetBindingContextType()
        => typeof(WorldLoadingPanelView);

        public int GetPriority()
        => 0;

        public async void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is WorldStateEvent)
            {
                var state = (WorldStateEvent)gameEvent;
                if (state.State == WorldState.loaded)
                {
                    if (LoadingPanel != null)
                    {
                        await UIController.Current.GoToPanelAsync(LoadingPanel);
                        LoadingPanel = null;
                    }
                }
            }
        }
    }
}