﻿using DG.Tweening;
using MVVMUI;
using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Kudos.MVVMUI
{
    public class SettingsPanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(SettingsViewModel);

        protected override async void OnAppearing()
        {
            var animated = transform.GetChild(0);
            var image = GetComponent<Image>();
            var oldY = animated.localPosition.y;
            animated.localPosition = new Vector3(animated.localPosition.x, Screen.height, animated.localPosition.z);
            _ = animated.DOLocalMoveY(oldY, 0.3f).SetEase(Ease.OutBack).AsyncWaitForCompletion();
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
            await image.DOFade(0.5f, 0.3f).AsyncWaitForCompletion();

            (BindingContext as SettingsViewModel).OnExitAnimation += SettingsPanelView_OnExitAnimation;
        }

        private async Task SettingsPanelView_OnExitAnimation()
        {
            var animated = transform.GetChild(0);
            var image = GetComponent<Image>();
            var oldY = animated.localPosition.y;
            _ = animated.DOLocalMoveY(Screen.height, 0.3f).AsyncWaitForCompletion();
            await image.DOFade(0, 0.3f).AsyncWaitForCompletion();
            animated.gameObject.SetActive(false);
            animated.localPosition = new Vector3(animated.localPosition.x, oldY, animated.localPosition.z);
        }

        protected override void OnDisappearing()
        {
            var animated = transform.GetChild(0);
            animated.gameObject.SetActive(true);
            (BindingContext as SettingsViewModel).OnExitAnimation -= SettingsPanelView_OnExitAnimation;
        }
    }
}