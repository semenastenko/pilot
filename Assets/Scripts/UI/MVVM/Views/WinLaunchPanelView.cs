﻿using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class WinLaunchPanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(WinLaunchViewModel);
    }
}