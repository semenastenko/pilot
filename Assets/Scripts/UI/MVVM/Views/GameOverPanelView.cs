﻿using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class GameOverPanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(GameEndViewModel);
    }
}