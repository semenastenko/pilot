﻿using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class GameplayPanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(GameplayPanelView);
    }
}