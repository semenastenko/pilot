﻿using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class PausePanelView : UIPanel
    {
        public override Type GetBindingContextType()
        => typeof(PausePanelView);
    }
}