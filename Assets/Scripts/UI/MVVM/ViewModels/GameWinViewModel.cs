﻿using DG.Tweening;
using Kudos.Enemy;
using Kudos.Events;
using MvvmHelpers;
using MvvmHelpers.Commands;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Kudos.MVVMUI
{
    [Injection]
    public class GameWinViewModel : ObservableObject, IGameEventHandler
    {
        private HUDViewModel _hud;
        private ComboHUDViewModel _combo;
        private EnemyMoveControl _enemy;
        public AsyncCommand RestartCommand { get; }

        float progressValue;

        public event Action<IGameEvent> OnEvent;

        public float ProgressValue
        {
            get => progressValue;
            set => SetProperty(ref progressValue, value);
        }

        string scoreText;

        public string ScoreText
        {
            get => scoreText;
            set => SetProperty(ref scoreText, value);
        }

        string levelText;

        public string LevelText
        {
            get => levelText;
            set => SetProperty(ref levelText, value);
        }

        public GameWinViewModel()
        {
            RestartCommand = new AsyncCommand(OnRestart);
        }

        [Inject]
        public void Init(HUDViewModel hud, ComboHUDViewModel combo, EnemyMoveControl enemy)
        {
            _hud = hud;
            _combo = combo;
            _enemy = enemy;
        }

        private async Task OnRestart()
        {
            OnEvent?.Invoke(new GameStateEvent(GameState.reset));
            await UIController.Current.GoToPanelAsync<WorldLoadingPanelView>(
                (nameof(WorldLoadingPanelView.LoadingPanel), typeof(MainMenuPanelView)));
        }

        public int GetPriority() => 0;

        public async void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is EndGameStateEvent)
            {
                var state = (EndGameStateEvent)gameEvent;
                if (state.State == WinState.winEnd)
                {
                    await UIController.Current.GoToPanelAsync<GameWinPanelView>();
                    ProgressValue = 0;
                    ScoreText = (int.Parse(_combo.ScoreText) + _enemy.HeightScore).ToString();
                    await DOTween.To(() => ProgressValue, x => ProgressValue = x, _hud.DistanceValue, 1.5f)
                        .SetEase(Ease.OutCubic)
                        .AsyncWaitForCompletion();
                }
            }
        }
    }
}