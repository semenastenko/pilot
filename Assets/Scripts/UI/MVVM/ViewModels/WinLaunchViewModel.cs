﻿using DG.Tweening;
using Kudos.Enemy;
using Kudos.Events;
using MvvmHelpers;
using MvvmHelpers.Commands;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;
using Zenject;

namespace Kudos.MVVMUI
{
    [Injection]
    public class WinLaunchViewModel : ObservableObject, IGameEventHandler
    {
        EnemyMoveControl _enemy;
        IDisposable timerDispose;
        IDisposable tapThresholdDispose;
        public Command TapCommand { get; }

        public event Action<IGameEvent> OnEvent;

        float progressValue;

        public float ProgressValue
        {
            get => progressValue;
            set => SetProperty(ref progressValue, value);
        }

        float timerValue;

        public float TimerValue
        {
            get => timerValue;
            set => SetProperty(ref timerValue, value);
        }

        bool isStarted;
        float time;
        bool wasTap;

        public float reduceForce = 0.5f;
        public float tapForce = 1.5f;
        public float fullTime = 3;
        public float tapThreshold = 0.1f;

        public WinLaunchViewModel()
        {
            TapCommand = new Command(OnTap);
        }

        [Inject]
        public void Init(EnemyMoveControl enemy)
        {
            _enemy = enemy;
        }

        private void OnTap()
        {
            if (!isStarted)
            {
                isStarted = true;
                timerDispose = Observable.EveryUpdate().Subscribe(_ => Update());
            }

            if (time < fullTime)
            {
                ProgressValue += tapForce * Time.deltaTime;
                wasTap = true;
                tapThresholdDispose?.Dispose();
                tapThresholdDispose = Observable.Timer(TimeSpan.FromSeconds(tapThreshold))
                    .Subscribe(_ => { wasTap = false; });
            }
        }

        private void Update()
        {
            if (!wasTap)
            {
                var subValue = reduceForce * Time.deltaTime;
                if (ProgressValue - subValue <= 0)
                {
                    ProgressValue = 0;
                }
                else
                {
                    ProgressValue -= subValue;
                }
            }

            if (time < fullTime)
            {
                time += Time.deltaTime;
            }
            else
            {
                timerDispose?.Dispose();
                _ = UIController.Current.GoToPanelAsync<GameplayPanelView>();
                _enemy.WinActionsEnd(ProgressValue);
            }

            TimerValue = time / fullTime;
        }

        public int GetPriority() => 0;

        public async void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is EndGameStateEvent)
            {
                var state = (EndGameStateEvent)gameEvent;
                if (state.State == WinState.winStart)
                {
                    isStarted = false;
                    time = 0;
                    ProgressValue = 0;
                    TimerValue = 0;
                    await UIController.Current.GoToPanelAsync<WinLaunchPanelView>();
                }
            }
        }
    }
}