﻿using Cysharp.Threading.Tasks;
using Kudos.Events;
using MvvmHelpers;
using MvvmHelpers.Commands;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using System.Threading.Tasks;

namespace Kudos.MVVMUI
{
    [Injection]
    public class SettingsViewModel : ObservableObject, IGameEventSender
    {
        public AsyncCommand ExitCommand { get; }
        public AsyncCommand AboutCommand { get; }
        public AsyncCommand<bool> MusicCommand { get; }
        public AsyncCommand<bool> EffectsCommand { get; }

        public event Action<IGameEvent> OnEvent;

        public event Func<Task> OnExitAnimation;

        public SettingsViewModel()
        {
            ExitCommand = new AsyncCommand(OnExit);
            AboutCommand = new AsyncCommand(OnAbout);
            MusicCommand = new AsyncCommand<bool>(OnMusic);
            EffectsCommand = new AsyncCommand<bool>(OnEffects);


        }

        private async Task OnExit()
        {
            await OnExitAnimation?.Invoke();
            await UIController.Current.GoToPanelAsync<MainMenuPanelView>();
        }

        private async Task OnAbout()
        {
            //await UIController.Current.GoToPanelAsync<MainMenuPanelView>();
        }

        private async Task OnMusic(bool value)
        {
            await UIController.Current.GoToPanelAsync<MainMenuPanelView>();
        }

        private async Task OnEffects(bool value)
        {
            await UIController.Current.GoToPanelAsync<MainMenuPanelView>();
        }

        public int GetPriority() => 0;
    }
}