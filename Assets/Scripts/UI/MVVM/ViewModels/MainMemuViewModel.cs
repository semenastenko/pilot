using Kudos.Events;
using Kudos.Logic;
using MvvmHelpers;
using MvvmHelpers.Commands;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Kudos.MVVMUI
{
    [Injection]
    public class MainMemuViewModel : ObservableObject, IGameEventSender
    {
        public AsyncCommand StartCommand { get; }
        public AsyncCommand SettingsCommand { get; }

        string levelText;

        public event Action<IGameEvent> OnEvent;

        public string LevelText
        {
            get => levelText;
            set => SetProperty(ref levelText, value);
        }

        public MainMemuViewModel()
        {
            StartCommand = new AsyncCommand(StartGame);
            SettingsCommand = new AsyncCommand(OpenSettings);

            
        }

        private async Task OpenSettings()
        {
            await UIController.Current.GoToPanelAsync<SettingsPanelView>();
        }

        private async Task StartGame()
        {
            await UIController.Current.GoToPanelAsync<GameplayPanelView>();
            OnEvent.Invoke(new GameStateEvent(GameState.begin));
        }

        public int GetPriority() => 0;
    }
}