﻿using DG.Tweening;
using Kudos.Events;
using MvvmHelpers;
using MvvmHelpers.Commands;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Kudos.MVVMUI
{
    [Injection]
    public class GameEndViewModel : ObservableObject, IGameEventHandler
    {
        private HUDViewModel _hud;
        private ComboHUDViewModel _combo;
        public AsyncCommand RestartCommand { get; }

        public event Action<IGameEvent> OnEvent;

        float progressValue;

        public float ProgressValue
        {
            get => progressValue;
            set => SetProperty(ref progressValue, value);
        }

        string scoreText;

        public string ScoreText
        {
            get => scoreText;
            set => SetProperty(ref scoreText, value);
        }

        string levelText;

        public string LevelText
        {
            get => levelText;
            set => SetProperty(ref levelText, value);
        }

        public GameEndViewModel()
        {
            RestartCommand = new AsyncCommand(OnRestart);
        }

        [Inject]
        public void Init(HUDViewModel hud, ComboHUDViewModel combo)
        {
            _hud = hud;
            _combo = combo;
        }

        private async Task OnRestart()
        {
            OnEvent?.Invoke(new GameStateEvent(GameState.reset));
            await UIController.Current.GoToPanelAsync<WorldLoadingPanelView>(
                (nameof(WorldLoadingPanelView.LoadingPanel), typeof(MainMenuPanelView)));
        }

        public int GetPriority() => 0;

        public async void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is EndGameStateEvent)
            {
                var state = (EndGameStateEvent)gameEvent;
                if (state.State == WinState.lose)
                {
                    await UIController.Current.GoToPanelAsync<GameOverPanelView>();
                    ScoreText = _combo.ScoreText;
                    ProgressValue = 0;
                    await DOTween.To(() => ProgressValue, x => ProgressValue = x, _hud.ProgressValue, 1.5f)
                        .SetEase(Ease.OutCubic)
                        .AsyncWaitForCompletion();
                }
            }
        }
    }
}