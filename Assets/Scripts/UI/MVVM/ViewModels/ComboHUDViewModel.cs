﻿using Kudos.Enemy;
using Kudos.Events;
using MvvmHelpers;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using Zenject;
using UniRx;
using UnityEngine;

namespace Kudos.MVVMUI
{
    [Injection]
    public class ComboHUDViewModel : ObservableObject, IGameEventHandler
    {
        EnemyMoveControl _enemy;
        IDisposable updateDispose;
        IDisposable comboDelayDispose;

        public event Action<IGameEvent> OnEvent;

        float comboProgressValue;

        public float ComboProgressValue
        {
            get => comboProgressValue;
            set => SetProperty(ref comboProgressValue, value);
        }

        string scoreText;

        public string ScoreText
        {
            get => scoreText;
            set => SetProperty(ref scoreText, value);
        }

        string comboText;

        public string ComboText
        {
            get => comboText;
            set => SetProperty(ref comboText, value);
        }

        int comboCount;
        int score;
        bool isComboDelay;
        float time;
        float scoreTimer;
        public float comboDelayTime = 5;

        public ComboHUDViewModel()
        {
            ComboProgressValue = 0;
            ScoreText = "0";
            ComboText = "1x";
        }

        [Inject]
        public void Init(EnemyMoveControl enemy)
        {
            _enemy = enemy;

            _enemy.OnArchTriggered += Enemy_OnArchTriggered;
            _enemy.OnAttackTriggered += Enemy_OnAttackTriggered;
        }

        private void Enemy_OnAttackTriggered()
        {
            ResetCombo();
        }

        private void Enemy_OnArchTriggered()
        {
            comboCount++;
            score += 10 * (1 + comboCount) * 2;
            time = 0;
            comboDelayDispose?.Dispose();
            ComboProgressValue = 0;
            comboDelayDispose = Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    time += Time.deltaTime;
                    if (time >= comboDelayTime)
                    {
                        ResetCombo();
                    }
                    else
                    {
                        ComboProgressValue = 1 - time / comboDelayTime;
                    }
                });
        }

        private void ResetCombo()
        {
            time = 0;
            comboDelayDispose?.Dispose();
            ComboProgressValue = 0;
            comboCount = 0;
        }

        private void Update()
        {
            scoreTimer += Time.deltaTime;
            if (scoreTimer >= 1)
            {
                scoreTimer = 0;
                score += 10 * (1 + comboCount);
            }

            ScoreText = score.ToString();
            if (comboCount != 0)
                ComboText = $"{comboCount + 1}x";
            else
                ComboText = "1x";
        }

        public int GetPriority() => -1;

        public void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is GameStateEvent)
            {
                var state = (GameStateEvent)gameEvent;
                if (state.State == GameState.begin)
                {
                    UIController.Current.AddHUD<ComboHUDView>();
                    updateDispose?.Dispose();
                    updateDispose = Observable.EveryUpdate().Subscribe(_ => Update());
                    scoreTimer = 0;
                    score = 0;
                    ResetCombo();
                }
                else if (state.State == GameState.end)
                {
                    score = 0;
                    scoreTimer = 0;
                    ResetCombo();
                    updateDispose?.Dispose();
                    UIController.Current.RemoveHUD<ComboHUDView>();
                }
            }
        }
    }
}