﻿using Kudos.Enemy;
using Kudos.Events;
using MvvmHelpers;
using MvvmHelpers.Commands;
using MVVMUI;
using MVVMUI.Zenject;
using System;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;
using UniRx;

namespace Kudos.MVVMUI
{
    [Injection]
    public class HUDViewModel : ObservableObject, IGameEventHandler
    {
        EnemyMoveControl _enemy;
        IDisposable updateDispose;

        public event Action<IGameEvent> OnEvent;
        
        float progressValue;

        public float ProgressValue
        {
            get => progressValue;
            set => SetProperty(ref progressValue, value);
        }

        float distanceValue;

        public float DistanceValue
        {
            get => distanceValue;
            set => SetProperty(ref distanceValue, value);
        }

        string timerText;

        public string TimerText
        {
            get => timerText;
            set => SetProperty(ref timerText, value);
        }

        string levelText;

        public string LevelText
        {
            get => levelText;
            set => SetProperty(ref levelText, value);
        }

        public HUDViewModel()
        {
            DistanceValue = 0;
            ProgressValue = 0;
        }

        [Inject]
        public void Init(EnemyMoveControl enemy)
        {
            _enemy = enemy;

        }

        private void Update()
        {
            var result = _enemy.GetDistanceInPercent();
            DistanceValue = result.value;
            ProgressValue = _enemy.DistanceProgress();
            if (result.outzoneTimer == 0)
            {
                TimerText = "";
            }
            else
            {
                TimerText = string.Format("{0:0.0}", result.outzoneMaxTimer - result.outzoneTimer);
            }
        }

        public int GetPriority() => -1;

        public void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is GameStateEvent)
            {
                var state = (GameStateEvent)gameEvent;
                if (state.State == GameState.begin)
                {
                    UIController.Current.AddHUD<HUDView>();
                    updateDispose?.Dispose();
                    updateDispose = Observable.EveryUpdate().Subscribe(_=> Update());
                }
                else if (state.State == GameState.end)
                {
                    updateDispose?.Dispose();
                    UIController.Current.RemoveHUD<HUDView>();
                }
            }
        }
    }
}