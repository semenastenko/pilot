﻿using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class ComboHUDView : UIHUD
    {
        public override Type GetBindingContextType()
        => typeof(ComboHUDViewModel);
    }
}