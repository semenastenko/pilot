﻿using MVVMUI;
using System;

namespace Kudos.MVVMUI
{
    public class HUDView : UIHUD
    {
        public override Type GetBindingContextType()
        => typeof(HUDViewModel);
    }
}