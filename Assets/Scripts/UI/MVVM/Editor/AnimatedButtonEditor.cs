﻿using UnityEditor;
using MVVMUI.Elements.Editor;

namespace Kudos.MVVMUI.Editor
{
    [CustomEditor(typeof(AnimatedButton))]
    public class AnimatedButtonEditor : TMPButtonEditor
    {
        protected SerializedProperty m_ButtonImageProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_ButtonImageProperty = serializedObject.FindProperty("m_buttonImage");
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            serializedObject.Update();
            EditorGUILayout.PropertyField(m_ButtonImageProperty);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
