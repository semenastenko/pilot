﻿using DG.Tweening;
using MVVMUI.Elements;
using UnityEngine;
using UnityEngine.UI;

namespace Kudos.MVVMUI
{
    public class AnimatedButton : TMPButton
    {
        [SerializeField] Image m_buttonImage;
        float animationDuration = 0.25f;

        protected override async void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);

            switch (state)
            {
                case SelectionState.Pressed:
                    await m_buttonImage.transform.DOLocalMoveY(-10, animationDuration).AsyncWaitForCompletion();
                    break;
                default:
                    await m_buttonImage.transform.DOLocalMoveY(0, animationDuration).AsyncWaitForCompletion();
                    break;
            }
        }
    }
}
