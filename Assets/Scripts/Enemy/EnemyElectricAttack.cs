using Kudos.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.Enemy
{
    public class EnemyElectricAttack : MonoBehaviour
    {
        private EnemyMoveControl _enemy;
        public static EnemyElectricAttack Create(EnemyMoveControl enemy, EnemyElectricAttack prefab, Vector3 position)
        {
            var randOffset = Random.rotation * Vector3.forward * Random.Range(1, 5) / 2;
            var attack = Instantiate(prefab, position + randOffset, Quaternion.identity);
            attack._enemy = enemy;
            return attack;
        }
        private void OnTriggerEnter(Collider other)
        {
            var player = other.GetComponent<PlayerPlane>();
            if (player != null)
            {
                player.DecreaseSpeed();
                _enemy.IncreaseDistance();
                Debug.Log($"EnemyElectricAttack {other.name}");
            }
        }
    }
}