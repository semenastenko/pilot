﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Kudos.Player
{
    public class CheckpointTrigger : MonoBehaviour
    {
        [SerializeField] ParticleSystem arcParticle;
        [SerializeField] ParticleSystem endParticle;

        public event Action<CheckpointTrigger> OnTrigger;

        private void OnTriggerEnter(Collider other)
        {
            //var obj = other.gameObject.GetComponent<PlayerPlane>();
            //if (obj == null) return;
            OnTrigger?.Invoke(this);
            arcParticle.gameObject.SetActive(false);
            endParticle.Play();

            Observable.Timer(System.TimeSpan.FromSeconds(endParticle.main.duration))
                .Subscribe(delegate
                {
                    if (this != null)
                        Destroy(this.gameObject);
                });
        }
    }
}
