using Kudos.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Kudos.Enemy
{
    public class RadarPoint : MonoBehaviour
    {
        [Inject] PlayerPlane player;
        [SerializeField] Transform target;
        private Camera _camera;
        private Transform heightVector;

        [SerializeField] float height = 3;
        [SerializeField] float offsetScale = 0.75f;
        [SerializeField] float arrowScale = 10;

        private void Start()
        {
            _camera = GetComponentInParent<Camera>();
            heightVector = transform.GetChild(0);
        }

        private void FixedUpdate()
        {
            transform.position = new Vector3(target.transform.position.x, _camera.transform.position.y - height, target.transform.position.z);
            transform.localScale = Vector3.one * _camera.orthographicSize / arrowScale;
            var andel = Vector3.SignedAngle(target.transform.forward, Vector3.forward, Vector3.up);

           //transform.localEulerAngles = Vector3.forward * andel;

            if (target.transform.position.y < player.transform.position.y)
            {
                heightVector.localEulerAngles = Vector3.forward * 180;
            }
            else
            {
                heightVector.localEulerAngles = Vector3.zero;
            }
        }
    }
}
