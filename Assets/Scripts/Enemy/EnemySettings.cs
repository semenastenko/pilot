﻿using Kudos.Player;
using UnityEngine;

namespace Kudos.Enemy
{
    [CreateAssetMenu(fileName = "EnemySettings", menuName = "SO/Plane/EnemySettings")]
    public class EnemySettings : ScriptableObject
    {
        [SerializeField] int finishDistance;
        [SerializeField] float startDistance;
        [SerializeField] float overtakeDistance;
        [SerializeField] float catchDistance;
        [SerializeField] float startSpeed;
        [SerializeField] float spawnInterval;
        [SerializeField] float attackInterval;
        [SerializeField] float outzoneMaxTime;
        [SerializeField] float outzoneDistanceFactor;
        [SerializeField] CheckpointTrigger archPrefab;
        [SerializeField] GameObject finishBeamPrefab;
        [SerializeField] EnemyElectricAttack electricAttackPrefab;
        [Header("Camera Settings")]
        [SerializeField] float cameraSmoothTime;
        [SerializeField] Vector3 cameraOffset;

        public float FinishDistance => finishDistance;
        public float StartDistance => startDistance;
        public float OvertakeDistance => overtakeDistance;
        public float CatchDistance => catchDistance;
        public float StartSpeed => startSpeed;
        public float SpawnInterval => spawnInterval;
        public float AttackInterval => attackInterval;
        public float OutzoneMaxTime => outzoneMaxTime;
        public float OutzoneDistanceFactor => outzoneDistanceFactor;
        public CheckpointTrigger ArchPrefab => archPrefab;
        public GameObject FinishBeamPrefab => finishBeamPrefab;
        public EnemyElectricAttack ElectricAttackPrefab => electricAttackPrefab;
        public float CameraSmoothTime => cameraSmoothTime;
        public Vector3 CameraOffset => cameraOffset;
    }
}