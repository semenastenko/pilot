using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UniRx;
using System;

public class HeightText : MonoBehaviour
{
    [SerializeField] TMP_Text text;

    public void SetText(string value)
    {
        text.text = value;
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform);
    }
}
