﻿using UnityEngine;

namespace Kudos.Enemy
{
    public class RadarPointerVector : MonoBehaviour
    {
        [SerializeField] Transform target;
        [SerializeField] Transform lookat;
        private Camera _camera;

        [SerializeField] float height = 3;
        [SerializeField] float offsetScale = 0.75f;
        [SerializeField] float arrowScale = 10;

        private void Start()
        {
            _camera = GetComponentInParent<Camera>();
        }

        private void FixedUpdate()
        {
            transform.position = new Vector3(target.transform.position.x, _camera.transform.position.y - height, target.transform.position.z);
            transform.localScale = Vector3.one * _camera.orthographicSize / arrowScale;
            var andel = Vector3.SignedAngle(lookat.position - target.position, Vector3.forward, Vector3.up);

            transform.localEulerAngles = Vector3.forward * andel;
        }
    }
}
