﻿using Cysharp.Threading.Tasks;
using Kudos.Player;
using System;
using UnityEngine;
using Zenject;
using UniRx;
using System.Collections.Generic;
using Kudos.Events;
using Kudos.Navigation;

namespace Kudos.Enemy
{
    [RequireComponent(typeof(Agent))]
    public class EnemyMoveControl : MonoBehaviour, IDisposable, IGameEventSender
    {
        [Inject] PlayerPlane _player;
        [Inject] CameraFollower _cameraFollower;
        [Inject] EnemySettings _enemySettings;
        Agent _Agent;
        [SerializeField] Rigidbody _pilotRigidbody;
        [SerializeField] GameObject _planeGameObject;
        [SerializeField] GameObject _pilotParticle;
        [SerializeField] ParticleSystem _planeParticle;
        [SerializeField] HeightText _heightText;

        private int moveCount = 0;
        private bool isPlay;
        private float distance;
        private Vector3 lastSpawnPosition;
        private Vector3 lastAttackPosition;
        private float lastDistanceProgress;
        private (Transform parent, Vector3 pos) pilotStartValue;
        private float outzoneTimer = 0;
        private List<CheckpointTrigger> spawnedTrigger;
        private Queue<CheckpointTrigger> removeTrigger;
        private GameObject spawnedFinish;
        private List<GameObject> spawnedMarks;

        public event Action<IGameEvent> OnEvent;
        public event Action OnArchTriggered;
        public event Action OnAttackTriggered;

        public int HeightScore { get; private set; }

        private void Start()
        {
            _Agent = GetComponent<Agent>();
            spawnedTrigger = new List<CheckpointTrigger>();
            removeTrigger = new Queue<CheckpointTrigger>();
            spawnedMarks = new();
            distance = _enemySettings.StartDistance;
            pilotStartValue = (_pilotRigidbody.transform.parent, _pilotRigidbody.transform.localPosition);
            moveCount = 0;
            gameObject.SetActive(false);
        }

        public async UniTask<AgentStatus> StartMove(Vector3 targetPosition)
        {
            if (moveCount == _enemySettings.FinishDistance - 1)
            {
                if (spawnedFinish != null)
                {
                    Destroy(spawnedFinish);
                }
                spawnedFinish = Instantiate(_enemySettings.FinishBeamPrefab, new Vector3(targetPosition.x, 250, targetPosition.z), Quaternion.Euler(90, 0, 0));
                //#if UNITY_EDITOR
                //                UnityEditor.EditorApplication.isPaused = true;
                //#endif
            }
            isPlay = true;

            gameObject.SetActive(true);
            _planeGameObject.SetActive(true);
            _pilotRigidbody.transform.SetParent(null);
            _pilotRigidbody.isKinematic = true;
            _pilotRigidbody.transform.SetParent(pilotStartValue.parent);
            _pilotRigidbody.transform.localPosition = pilotStartValue.pos;
            _pilotRigidbody.transform.localEulerAngles = Vector3.zero;

            var status = _Agent.FindPath(targetPosition);

            Debug.Log($"StartMove Status {status}");
            while (_Agent.Status == AgentStatus.InProgress)
            {
                if (this == null) break;
                if ((transform.position - lastSpawnPosition).magnitude > _enemySettings.SpawnInterval)
                {
                    lastSpawnPosition = transform.position;
                    var arch = Instantiate(_enemySettings.ArchPrefab, transform.position - transform.forward * 2, transform.rotation);
                    arch.OnTrigger += OnTriggerArch;
                    spawnedTrigger.Add(arch);
                }
                if ((transform.position - lastAttackPosition).magnitude > _enemySettings.AttackInterval)
                {
                    EnemyElectricAttack.Create(this, _enemySettings.ElectricAttackPrefab, _player.transform.position + (_player.transform.forward + Vector3.forward) * 10);
                    lastAttackPosition = transform.position;
                }

                foreach (var item in spawnedTrigger)
                {
                    if (_player.transform.position.z - item.transform.position.z > 10)
                    {
                        removeTrigger.Enqueue(item);
                    }
                }

                while (removeTrigger.Count != 0)
                {
                    var removed = removeTrigger.Dequeue();
                    spawnedTrigger.Remove(removed);
                    Destroy(removed.gameObject);
                }

                if (transform.position.z - _player.transform.position.z > 0)
                {
                    _Agent.Speed = _enemySettings.StartSpeed * distance / (transform.position - _player.transform.position).magnitude;
                }
                else
                {
                    _Agent.Speed = _enemySettings.StartSpeed * (1 + (transform.position - _player.transform.position).magnitude / distance);
                }
                await UniTask.Yield();
            }

            if (_Agent.Status != AgentStatus.Invalid)
            {
                moveCount++;
            }
            if (moveCount >= _enemySettings.FinishDistance)
            {
                Debug.Log("<color=yellow>Finish</color>");
                Dispose();
                WinActionsStart();
            }
            return _Agent.Status;
        }

        private void OnTriggerArch(CheckpointTrigger arch)
        {
            if (distance > _enemySettings.CatchDistance)
            {
                distance -= _enemySettings.OvertakeDistance;
                _player.IncreaseSpeed();
            }
            OnArchTriggered?.Invoke();
            spawnedTrigger.Remove(arch);
        }

        public void IncreaseDistance()
        {
            distance += _enemySettings.OvertakeDistance;
            OnAttackTriggered?.Invoke();
        }

        private async void WinActionsStart()
        {
            OnEvent?.Invoke(new GameStateEvent(GameState.end));
            _pilotRigidbody.transform.SetParent(null);

            _cameraFollower.SetSettings(_pilotRigidbody.transform, Vector3.zero, _pilotRigidbody.transform, _enemySettings.CameraOffset, _enemySettings.CameraSmoothTime);
            await _cameraFollower.LerpTarget(_pilotRigidbody.transform, _planeParticle.main.duration);

            OnEvent?.Invoke(new EndGameStateEvent(WinState.winStart));
        }

        public async void WinActionsEnd(float force)
        {
            _planeParticle.Play();
            _planeGameObject.SetActive(false);
            _pilotParticle.gameObject.SetActive(true);

            _pilotRigidbody.isKinematic = false;
            var timer = 0f;
            var nextHeight = _pilotRigidbody.position.y + 10;
            var count = 100;
            while (timer < 5)
            {
                if (_pilotRigidbody.position.y >= nextHeight)
                {
                    var mark = Instantiate(_heightText, _pilotRigidbody.position + Vector3.right, Quaternion.identity);
                    mark.SetText(count.ToString());
                    spawnedMarks.Add(mark.gameObject);
                    nextHeight += 10;
                    count += 100;
                }
                _pilotRigidbody.AddForce(Vector3.up * Time.deltaTime * 500 * (1 + force) * (2 - distance / _enemySettings.StartDistance));
                timer += Time.unscaledDeltaTime;
                await UniTask.Yield();
            }
            _pilotRigidbody.velocity = Vector3.zero;
            _pilotRigidbody.AddTorque(Vector3.right * 10);
            _pilotParticle.gameObject.SetActive(false);
            _cameraFollower.SetSettings(_pilotRigidbody.transform, Vector3.zero, null);

            HeightScore = count;
            OnEvent?.Invoke(new EndGameStateEvent(WinState.winEnd));
        }

        public (float value, float outzoneTimer, float outzoneMaxTimer) GetDistanceInPercent()
        {
            var halfway = _enemySettings.StartDistance - _enemySettings.CatchDistance;
            var distance = (transform.position - _player.transform.position).magnitude - _enemySettings.CatchDistance;
            var result = Mathf.Clamp01(1 - distance / (_enemySettings.OutzoneDistanceFactor * halfway));
            if (result == 0 && isPlay)
                OutzoneTime();
            else
                outzoneTimer = 0;
            return (result, outzoneTimer, _enemySettings.OutzoneMaxTime);
        }

        public float DistanceProgress()
        {
            var stageValue = moveCount / _enemySettings.FinishDistance;
            var totalDistance = (_Agent.EndPosition - _Agent.StartPosition).magnitude;
            var agentDistance = (_Agent.transform.position - _Agent.StartPosition).magnitude;
            var agentProgress = 0f;
            if (totalDistance > 0)
                agentProgress = agentDistance / totalDistance;

            agentProgress = Mathf.Clamp01(agentProgress);

            if (isPlay)
                lastDistanceProgress = stageValue + agentProgress / _enemySettings.FinishDistance;
            return lastDistanceProgress;
        }

        public void OutzoneTime()
        {
            outzoneTimer += Time.deltaTime;
            if (outzoneTimer >= _enemySettings.OutzoneMaxTime)
            {
                outzoneTimer = 0;
                _player.Die();
            }
        }

        public void Dispose()
        {
            isPlay = false;
            moveCount = 0;
            while (spawnedTrigger.Count != 0)
            {
                var removed = spawnedTrigger[0];
                spawnedTrigger.Remove(removed);
                if (removed != null)
                    Destroy(removed.gameObject);
            }
            if (spawnedFinish != null)
            {
                Destroy(spawnedFinish);
            }
            foreach (var item in spawnedMarks)
            {
                if (item != null)
                    Destroy(item);
            }
            spawnedMarks.Clear();
            lastSpawnPosition = Vector3.zero;
            lastAttackPosition = Vector3.zero;
            _Agent?.Dispose();
            distance = _enemySettings.StartDistance;
            gameObject.SetActive(false);
        }
    }
}