using Kudos.Navigation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationDebug : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Pathfinding.Instance.DrawGizmos();
        }
    }
}
