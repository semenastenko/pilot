using System.Collections;
using System.Collections.Generic;
using System;

namespace Kudos.Logic
{
    public static class SeedRandom
    {
        public static string seed;

        public static void RandomizeSeed() => seed = DateTime.Now.GetHashCode().ToString();

        public static Random GetRandom() => new Random(seed.GetHashCode());
        public static Random GetRandom(string customSeed) => new Random(customSeed.GetHashCode());

    }
}