using Kudos.Events;
using Kudos.MVVMUI;
using SaveCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.Logic
{
    [Serializable]
    public class ScoreSave : ISaveable
    {
        public int level;
        public int score;

        public void UpdateData(object data)
        {
            var obj = data as ScoreSave;
            level = obj.level;
            score = obj.score;
        }
    }
    public class ScoreController : SaveLoad<ScoreSave>, IGameEventReceiver
    {
        private ScoreSave save;
        private MainMemuViewModel _mainMenu;
        private HUDViewModel _gameplay;
        private GameEndViewModel _gameEnd;
        private GameWinViewModel _gameWin;
        private OnSaveEvent _saveEvent;
        public ScoreController(MainMemuViewModel mainMenu, 
            HUDViewModel gameplay,
            GameEndViewModel gameEnd,
            GameWinViewModel gameWin,
            OnSaveEvent saveEvent)
        {
            _saveEvent = saveEvent;
            _mainMenu = mainMenu;
            _gameplay = gameplay;
            _gameEnd = gameEnd;
            _gameWin= gameWin;
        }

        public int GetPriority()
        => 0;

        public void NextLevel()
        {
            save.level += 1;
            _mainMenu.LevelText = $"LEVEL {save.level + 1}";
            _gameplay.LevelText = $"{save.level + 1}";
            _gameEnd.LevelText = $"{save.level + 1}";
            _gameWin.LevelText = $"{save.level + 1}";
        }

        public void ReceiveEvent(IGameEvent gameEvent)
        {
            if (gameEvent is EndGameStateEvent)
            {
                var state = (EndGameStateEvent)gameEvent;
                if (state.State == WinState.winEnd)
                {
                    NextLevel();
                    _saveEvent.Invoke(true);
                }
            }
        }

        protected override void OnLoad(ScoreSave data)
        {
            if (data != null)
                save = data;
            else
                save = new();

            _mainMenu.LevelText = $"LEVEL {save.level + 1}";
            _gameplay.LevelText = $"{save.level + 1}";
            _gameEnd.LevelText = $"{save.level + 1}";
            _gameWin.LevelText = $"{save.level + 1}";
        }

        protected override ScoreSave OnSave()
        {
            return save;
        }
    }
}