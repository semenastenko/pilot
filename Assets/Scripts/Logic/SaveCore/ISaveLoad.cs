﻿using System;

namespace SaveCore
{
    public interface ISaveable
    {
        void UpdateData(object data);
    }

    public interface ISaveLoad
    {
        ISaveable OnSave();

        void OnLoad(ISaveable data);

        Type SaveableType();
    }
}
