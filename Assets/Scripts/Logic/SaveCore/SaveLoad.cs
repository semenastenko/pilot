﻿using System;

namespace SaveCore
{
    public abstract class SaveLoad<Data> : ISaveLoad
        where Data : class, ISaveable
    {
        void ISaveLoad.OnLoad(ISaveable data) => OnLoad(data as Data);

        ISaveable ISaveLoad.OnSave() => OnSave();

        protected abstract void OnLoad(Data data);

        protected abstract Data OnSave();

        public Type SaveableType() => typeof(Data);

    }
}
