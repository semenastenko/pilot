﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace SaveCore
{
    public class OnSaveEvent : UnityEvent<bool>
    {

    }

    public class SaveLoadController
    {
        private string filePath = Application.persistentDataPath + "/Save.save";
        [Serializable]
        protected class Save
        {
            public List<ISaveable> data = new List<ISaveable>();
        }

        private ISaveLoad[] _saveLoads;
        private Save save;
        private bool isSaving;

        public SaveLoadController(ISaveLoad[] saveLoads, OnSaveEvent saveEvent)
        {
            _saveLoads = saveLoads;
            LoadData();

            saveEvent.AddListener(OnSave);
        }

        private void OnSave(bool value)
        {
            if (value)
                SaveData();
        }

        public async void SaveData()
        {
            await UniTask.WaitWhile(() => isSaving);
            isSaving = true;
            foreach (var item in _saveLoads)
            {
                var saveable = save.data.Find(x => x.GetType() == item.SaveableType());
                if (saveable != null)
                    saveable.UpdateData(item.OnSave());
                else
                    save.data.Add(item.OnSave());
            }
            var data = BinaryFormatterUtility.ToBinary(save);
            using (StreamWriter sw = new StreamWriter(filePath, false))
            {
                await sw.WriteAsync(data);
            }
            isSaving = false;
        }

        private void LoadData()
        {
            var data = "";
            if (File.Exists(filePath))
            {
                using (StreamReader sw = new StreamReader(filePath))
                {
                    data = sw.ReadToEnd();
                }
            }
            if (string.IsNullOrEmpty(data))
            {
                save = new Save();
            }
            else
            {
                save = BinaryFormatterUtility.FromBinary<Save>(data);
            }

            foreach (var item in _saveLoads)
            {
                var saveable = save.data.Find(x => x.GetType() == item.SaveableType());
                item.OnLoad(saveable);
            }
        }

        public void ClearData()
        {
            save.data = new List<ISaveable>();
            var data = BinaryFormatterUtility.ToBinary(save);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            //SaveData();
            LoadData();
        }
    }
}
