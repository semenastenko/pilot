using UnityEngine;
using UnityEngine.Events;
using Zenject;
using System;
using Cysharp.Threading.Tasks;
using MVVMUI;
using Kudos.MVVMUI;
using Kudos.Events;
using MVVMUI.Zenject;

namespace Kudos.Installers
{
    public class UIInstaller : MonoInstaller
    {
        [Inject] Canvas _canvas;
        public override void InstallBindings()
        {
            var navigation = new UIController(_canvas);

            foreach (var panel in navigation.Bindables)
            {
                if (panel.BindingContext is IGameEventSender)
                {
                    Container.BindInterfacesAndSelfTo<IGameEventSender>().FromInstance((IGameEventSender)panel.BindingContext);
                }
                if (panel.BindingContext is IGameEventReceiver)
                {
                    Container.BindInterfacesAndSelfTo<IGameEventReceiver>().FromInstance((IGameEventReceiver)panel.BindingContext);
                }

                Container.BindBindingContext(panel.BindingContext);
            }   
        }

        public override void Start()
        {
            base.Start();
            UIController.Current.SetActive<WorldLoadingPanelView>((nameof(WorldLoadingPanelView.LoadingPanel), typeof(MainMenuPanelView)));
        }
    }
}