using Kudos.Enemy;
using Kudos.Player;
using Kudos.SimpleGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Kudos.Installers
{
    [CreateAssetMenu(fileName = "SOInstaller", menuName = "SO/Installers/SOInstaller")]
    public class SOInstaller : ScriptableObjectInstaller<SOInstaller>
    {
        public PlayerSettings planeSettings;
        public EnemySettings enemySettings;
        public ChunkPlacerSettings chunkPlacerSettings;
        public RandomSettings randomSettings;

        public override void InstallBindings()
        {
            Container.BindInstances(planeSettings);
            Container.BindInstances(enemySettings);
            Container.BindInstances(chunkPlacerSettings);
            Container.BindInstances(randomSettings);
        }
    }
}
