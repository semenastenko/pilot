using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Kudos.Managers;
using Kudos.SimpleGeneration;
using Kudos.Navigation;
using Kudos.Events;
using SaveCore;
using Kudos.Logic;

namespace Kudos.Installers
{
    public class GameInstaller : MonoInstaller
    {
        private OnSaveEvent _onSaveEvent;
        [SerializeField] bool isRandomSeed;
        [SerializeField] string seed;
        public override void InstallBindings()
        {
            Application.targetFrameRate = 60;
            if (isRandomSeed)
            {
                SeedRandom.RandomizeSeed();
            }
            else
            {
                SeedRandom.seed = seed;
            }
            _onSaveEvent = new OnSaveEvent();

            Container.BindInterfacesAndSelfTo<GameEventSystem>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<Input.ScreenControl>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<WorldBuilder>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<NavigationController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<ScoreController>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<SaveLoadController>().AsSingle().NonLazy();
            Container.Bind<OnSaveEvent>().FromInstance(_onSaveEvent);
        }

        private void OnApplicationPause(bool pause)
        {
            _onSaveEvent.Invoke(pause);

        }

#if UNITY_EDITOR
        private void OnApplicationQuit()
        {
            _onSaveEvent.Invoke(true);
        }
#endif
    }
}
