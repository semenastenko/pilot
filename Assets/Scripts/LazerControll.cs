using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPEW.ShadersAPI
{

    /// <summary>
    /// ������ ���������� �������
    /// </summary>
    public class LazerControll : MonoBehaviour
    {
        [SerializeField] float distanceRay = 1;
        [SerializeField] Color lazerColor;
        [SerializeField] float emissionLazer = 1;

        MaterialPropertyBlock _materialProperties;
        Transform _firePoint;
        LineRenderer _lineRenderer;
        GameObject _startVFX;
        GameObject _endVFX;
        List<ParticleSystem> _particlesLazer;

        /// <summary>
        /// �������� ��� ���������� ������� ����
        /// </summary>
        public float DistanceRay
        {
            get { return distanceRay; }

            set
            {
                if (value < 0) value = 0;
                distanceRay = value;
            }
        }

        /// <summary>
        /// ����� ��������� ����� ������
        /// </summary>
        /// <param name="color">���� ������</param>
        /// <param name="intensity">������� �����</param>
        public void ChangeColor(Color color, float intensity = 1f)
        {
            _materialProperties.SetColor("_ColorLaz", color);
            _materialProperties.SetFloat("_EmissionLaz", intensity);
            _lineRenderer.SetPropertyBlock(_materialProperties);
            //Shader.SetGlobalColor("_ColorLaz", color);
            //Shader.SetGlobalFloat("_EmissionLaz", intensity);

            for (int i = 0; i < _particlesLazer.Count; i++)
            {
                _particlesLazer[i].gameObject.GetComponent<ParticleSystem>().startColor = color;
            }
        }

        private void Awake()
        {
            _particlesLazer = new List<ParticleSystem>();
            _materialProperties = new MaterialPropertyBlock();

            _firePoint = gameObject.transform.GetChild(0);
            _startVFX = gameObject.transform.GetChild(1).gameObject;
            _endVFX = gameObject.transform.GetChild(2).gameObject;
            _lineRenderer = gameObject.transform.GetChild(3).GetComponent<LineRenderer>();
        }

        void Start()
        {
            FillList();
            EnableLazer();
            ChangeColor(lazerColor, emissionLazer);
        }

        // Update is called once per frame

        private void FixedUpdate()
        {
            if (distanceRay > 0 && _lineRenderer.enabled == false)
            {
                EnableLazer();
            }

            if (distanceRay <= 0)
            {
                DisableLazer();
            }

            UpdateLazer();

            //if (Input.GetKey(KeyCode.W))
            //{
            //    DistanceRay += 0.1f;
            //}

            //if (Input.GetKey(KeyCode.S))
            //{
            //    DistanceRay -= 0.1f;
            //}
        }

        private void UpdateLazer()
        {
            //var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

            Vector3 endPoint = _firePoint.position + _firePoint.forward * distanceRay;

            _startVFX.transform.position = _firePoint.position;

            _lineRenderer.SetPosition(0, _firePoint.position);
            _lineRenderer.SetPosition(1, endPoint);

            //var direction = endPoint - _firePoint.position;

            //RaycastHit hit;

            //if (Physics.Raycast(_firePoint.position, direction, out hit, direction.magnitude))
            //{
            //    _lineRenderer.SetPosition(1, hit.point);
            //}

            _endVFX.transform.position = _lineRenderer.GetPosition(1);
        }


        private void DisableLazer()
        {
            _lineRenderer.enabled = false;

            for (int i = 0; i < _particlesLazer.Count; i++)
            {
                _particlesLazer[i].Stop();
            }
        }

        private void EnableLazer()
        {
            _lineRenderer.enabled = true;

            for (int i = 0; i < _particlesLazer.Count; i++)
            {
                _particlesLazer[i].Play();
            }
        }


        private void FillList()
        {

            for (int i = 0; i < _startVFX.transform.childCount; i++)
            {
                var ps = _startVFX.transform.GetChild(i).GetComponent<ParticleSystem>();
                if (ps != null)
                {
                    _particlesLazer.Add(ps);
                }
            }

            for (int i = 0; i < _endVFX.transform.childCount; i++)
            {
                var ps = _endVFX.transform.GetChild(i).GetComponent<ParticleSystem>();
                if (ps != null)
                {
                    _particlesLazer.Add(ps);
                }
            }
        }
    }
}
